#!/bin/bash

rm -rf *.class
VAR=$1
javac $1 -Xdiags:verbose -Xlint:deprecation && java ANTLR4Ctrl ${VAR::5}
