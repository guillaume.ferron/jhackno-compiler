#!/bin/bash

PATH2FILES="../Src"
TMP="tmp"
DISTFOLDER="Dist"
ARCHIVESFOLDER="Archives"
JARNAME="Fred"
VERSION="$1_$(date '+%Y%m%d%H%M')"

cd $PATH2FILES
#Hook
cd ../Setup
export CLASSPATH=.:$(pwd)/antlr-4.7.2-complete.jar:$(pwd)/sqlite-jdbc-3.27.2.1.jar:$CLASSPATH
cd $PATH2FILES

mkdir -p $TMP && rm -rf $TMP/*

/usr/bin/javac -d $TMP *.java
/usr/bin/javac -d $TMP View/Components/*.java
/usr/bin/javac -d $TMP View/*.java
/usr/bin/javac -d $TMP Control/*.java
/usr/bin/javac -d $TMP Model/*.java
/usr/bin/javac -d $TMP Compiler/Jakn0/*.java
/usr/bin/javac -d $TMP Compiler/*.java
/usr/bin/javac -d $TMP Exceptions/*.java
/usr/bin/javac -d $TMP Tests/*.java
/usr/bin/javac -d $TMP Interfaces/*.java

mkdir -p $TMP/Images
/bin/cp Images/* $TMP/Images

/bin/cp Fred.db $TMP

/bin/cp ../Setup/*.jar $TMP
cd $TMP
/bin/echo "n" | unzip '*.jar'
rm -rf META-INF/
cd ..
echo -ne "Manifest-Version: 1.0\nMain-Class: Fred\n" > Manifest
/usr/bin/jar cvfm Fred.jar Manifest -C $TMP .

mkdir -p ../$ARCHIVESFOLDER
/bin/mv Manifest $TMP
/bin/cp Fred.jar ../$ARCHIVESFOLDER/Fred-$VERSION.jar
/bin/mv Fred.jar $TMP

mkdir -p ../$DISTFOLDER
mv $TMP ../$DISTFOLDER/Fred-$VERSION

echo
echo "Fred-$VERSION.jar has been generated in ../$DISTFOLDER/Fred-$VERSION/Fred.jar"
echo "Fred-$VERSION.jar has been copied in ../$ARCHIVESFOLDER/Fred-$VERSION.jar"
echo
echo Execute either : java -jar ../$DISTFOLDER/Fred-$VERSION/Fred.jar
echo Or : java -jar ../$ARCHIVESFOLDER/Fred-$VERSION.jar
echo
echo Done
