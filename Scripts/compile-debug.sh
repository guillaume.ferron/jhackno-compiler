#!/bin/bash

rm -rf *.class
javac $1 -Xdiags:verbose -Xlint:deprecation && java ANTLR4Ctrl ${$1::-4}
