#!/bin/bash

PATH="../Src"

cd $PATH
/usr/bin/find . -name "*.java" -type f -exec /usr/bin/vim {} \;
