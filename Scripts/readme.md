### Les scripts utilisés ici :

- build-dist.sh                 : créé une distribution à partir des Src/ existantes
    - Prend un argument "nom\_de\_larchive"

- clean-class.sh                : nettoie les .class dans Src/

- comment-packages.sh           : DEPRECATED

- compile-debug-halfbuggy.sh    : DEPRECATED

- compile-debug.sh              : DEPRECATED

- open-all.sh                   : open all project script, one by one, with vim

- review-archives.sh            : java -jar Archives/\*.jar

- save.sh                       : git the project

