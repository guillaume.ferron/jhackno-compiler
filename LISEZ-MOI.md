# FRED Documentation

# HOW TO START

```
cd Src/ && javac Fred.java && java Fred

```

Or, go in /Archives and execute last jar


## Bilan

Projet livré avec 10 jours ouvrés de retard.

A l'origine nous devions fournir un compilateur d'un certain 
pseudo code. Par manque de temps nous nous limitons à une 
calculatrice. Le principe reste le même.

Nous nous sommes attachés à documenter au maximum les différentes
parties du projet. Mais le travail est immense et il y a encore
beaucoup de travail à accomplir de ce coté là. À ce stade presque tout
le système est documenté, et beaucoup du reste est détaillé ou conservé simplifié.

En termes de fonctionnalité, le logiciel vient avec:
- un système d'utilisateur complet et sécurisé
- une base de donnée embarquée
- un gestionnaire de Jpanel
- un constructeur de pages
- un système de tests prêt à l'emploi
- un lien direct avec ANTLR4
- une calculatrice gérant les 4 opérations

En terme de qualité de code, nous avons essayé de coller au standard MVC.
Au point le plus strict puisque certains controleurs n'ont comme seul but
que de proxyfier un objet de référence situé dans le Model, pour les élé-
ments de la vue.

Le logiciel est livré clé en main, documenté et libre. Il a vocation a être amélioré puis distribué
aux étudiants de la prochaine promotion MIMO.

Les images présents dans le dossier Images/ ne sont pas particulièrement libre de droit et sont tenues à votre discrétion. Les photos destiné au logiciel se trouvent dans un téléphone qui, actuellement, reste introuvable.

Malgré le retard nous esperons que ce projet saura attirer votre attention et contribuer malgré tout à notre note.


#### Règles

- Règle concernant les commentaires
```
/**

*/
```
*toujours au dessus de classe ou methode.*


- Usage des annotations
`@param level`


#### readme.md
Les fichiers readme.md explicitent les scripts .sh que l'on trouve parfois dans les sources Src/

## Mode d'emploi

Les fichiers en développement sont dans ./Src
Les versions livrables sont dans ./Dist et ./Archives

Préparation de l'environnement
- cd ./Setup && ./setEnv.sh
- Executez la commande donnée
- cd ..

Exécution des sources
- cd ./Src
- javac Fred.java
- java Fred

Exécution d'un livrable
- cd ./Dist/Fred-.\*
- javac Fred.java
- java Fred

Exécution d'un jar dans un livrable
- cd ./Dist/Fred-.\*
- java -jar Fred.jar

Exécution d'une archive
- cd ./Archives
- java -jar Fred-.\*


#### Système de fichier

Fred : Dossier du projet
├── Archives : Package contenant les jar historicisés du projet
│   └── Fred-20190723114538.jar : Exemple de jar
│   └── ... Pour executer un jar : java -jar monjar.jar
├── Dist : Contient tous les fichiers .java à un instant i
│   └── Fred-20190723-114538 : Exemple de distribution
│       ├── ... Pour compiler une distribution : javac Fred.java
│           └── ... Pour executer : java Fred
├── Documentation : Contient toute la documentation relative au projet
│   ├── ANTLR4 : Contient un starter pour utiliser ANTLR4
│   │   ├── exemples : Contient des exemples de réalisation
│   │   │   └── calculator : Exemple type
│   │   │       ├── Calc.g4  : Fichiers contenant la grammaire
│   │   │       ├── Calc.java : Classe principale
│   │   │       ├── CalcLexers.g4 : Fichier de grammaire dissocié
│   │   │       ├── clean.sh : Script bash visant à recommencer l'exemple
│   │   │       ├── compile.sh : Script bash compilant tout ce qui doit l'être
│   │   │       ├── CustomCalcVisitor.java : Parser de type "Visiteur"
│   │   │       ├── Readme.md : Informations en anglais pour lancer le projet
│   │   │       ├── run.sh : Script bash pour lancer ANTLR4
│   │   │       └── testfile : Fichier contenant le code à parser
│   │   └── tutoriel.md : Tutoriel ANTLR4 portant sur la construction d'une grammaire (TODO)
│   └── Fred : Documentation spécifique au projet
│       ├── contexte.md : Histoire du projet (TODO)
│       ├── programme.md : Fonctionnement du projet (TODO)
│       └── projet.md : Historique du projet (TODO)
├── Src : Contient le code en développement
    ├── Fred.java  : Class principale
    └── X : Package contenant les instructions d'affichages
        Tests : Package contenant les tests du logiciel
        Database : Package contenant les données relatives au Modèle
        Compiler : package contenant les fichiers d'execution de ANTLR4
├── Scripts : Scripts bash automatisant certaines tâches répétitives
│   ├── build-dist.sh : Construit une dist du dossier Fred
│   ├── clean-class.sh : Nettoie les .class du dossier Fred
│   ├── comment-packages.sh : Commente l'instruction "package" dans tous les fichiers du dossier Fred
│   └── open-all.sh : Ouvre tous les scripts du projet dans vim
├── Setup : Instruction de configuration et librairies essentielles
│   ├── antlr-4.7.2-complete.jar : Librairie ANTLR
│   ├── setEnv.sh : Instruction pour exporter $CLASSPATH
│   └── sqlite-jdbc-3.27.2.1.jar : Librairie SQL

18 directories, 62 files
