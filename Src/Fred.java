/*
 * Fred, a Jakn0 compiler
 */

// UI/UX lib
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

// Standard lib
import java.util.*;

// Project lib
import Interfaces.*;
import Model.*;
import Control.*;
import Compiler.*;
import Tests.*;
import View.*;
import View.Components.*;

/**
 * @Fred
 *******************************
 *
 * Fred est un compilateur de pseudo code destiné aux étudiants du Master MIMO, La Sorbonne, afin de les aider dans leur apprentissage de l'algorithmie. Fred implémente le logiciel de compilation ANTLR4. Il est capable d'authentifier un utilisateur puis de le rediriger vers un gestionnaire de fenêtre simplifié.
 *
 */

@SuppressWarnings("deprecation")
public class Fred implements ActionListener {

    // Model
    User USER;

    // Control
    Xcore X;
    Database DB;
    CompilerEngine Compiler;

    // Pages
    MainX mx_built;
    Login login_built;
    Register register_built;
    Success success_built;
    Failure failure_built;
    Userboard ub_built;

    // Forms
    RegisterForm registerform;

    // Components
    JButton login;
    JButton toregister;
    JButton register;
    JButton tologin;
    JButton back1;
    JButton back2;
    JButton eval;
    JButton compile;
    JButton export;
    JButton exit;
    JButton canceledit;
    JButton validedit;
    JButton edituser;

    // Personal Data
    UserData userdata;
    JPasswordField lpass;
    JTextField lemail;
    JPasswordField rpass;
    JTextField remail;
    JTextField rname;
    JTextArea code;
    JLabel level;
    JLabel score;
    JPasswordField pass1;
    JPasswordField pass2;
    JTextField ubmail;
    JTextField ubname;

    // Classes de Tests
    TestUnit UnitTest;
    UseCase UseTest;

    public Fred(String[] args) {


        /**
         * @Introduction
         *
         * Le logiciel Fred est découpé en deux sous-ensembles d'utilisation :
         * - En ligne de commande
         * - Dans un affichage graphique
         *
         * La classe Fred est relié à deux sous-ensemble de fonctionnalités:
         * - Un compilateur (Compiler) basé sur ANTLR4
         * - Un systeme graphique (Xcore)
         *
         * Physiquement, les sources sont constitués de 8 dossiers :
         * - Compiler : regroupe les classes qui implémentent ANTLR4
         * - Exceptions : regroupe les classes qui implémentent des erreurs spécifques à Fred
         * - Images : regroupe les images utilisées par Fred
         * - Tests : regroupe les classes chargées des tests de Fred
         * - View : regroupe les class vues de Fred
         * - Model : regroupe les class modèles de Fred
         * - Control : regroupe les singletons controleurs de Fred
         * - Interfaces : regroupe les interfaces de Fred
         *
         * La classe Fred est construite autour de 11 modèles :
         * - User  : spécification d'un "Utilisateur"
         * - Admin  : spécification d'un "Administrateur"
         * - Data : spécification d'une "Donnée personnelle
         * - DataEmail : spécification d'un Email
         * - DataPass : spécification d'un Mot de passe
         * - DataScore : spécification d'un Score
         * - Meta : objet d'information sur le système
         * - Screen : spécification d'un "Ecran utilisateur"
         * - PseudoCode : spécification d'un "Pseudocode" injecté par l'utilisateur
         * - RegisterForm : container qui regroupe les informations d'enregistrement
         * - Screen : container qui encapsule une Page
         *
         * Affiche 8 vues :
         * - Register : spécification d'un "Ecran d'inscription"
         * - Login : spécification d'un "Ecran de connexion"
         * - MainX : spécification d'un "Ecran principal"
         * - Success : spécification d'un "Ecran de réussite de compilation"
         * - Failure : spécification d'un "Ecran d'échec de compilation"
         * - Page : spécification abstraite d'une "Page"
         * - Userboard : spécification d'une page de "Configuration utilisateur"
         * - TODO : Unregister : spécification d'un "Ecran de désenregistrement"
         *
         * Opère 3 controleurs et 2 containers de données froides:
         * - Config : objet de configuration proxyfiant Meta pour la vue
         * - UserData : conteneur de données personnelle froides pour la vue
         * - Database : connexion à la base de donnée
         * - RGPDReference : réalisation des actions RGPD sur les données personnelles
         * - Xcore : moteur graphique de l'application
         *
         * Implemente 13 interfaces
         * - DatabaseSetup : mettre en route la base de donnée
         * - Dimensions: gérer ses dimensions (largeur, hauteur)
         * - EditFile : écrire dans un fichier
         * - Encryption : encrypter des chaines de caractères
         * - GetFile : lire un fichier
         * - ImageManager : transformer des images en objet
         * - Layout : actions sur le Layout
         * - PersonalData : actions sur les données personnelles
         * - SecData : actions de sécurisation des données
         * - SecMail : action de sécurisation des adresses email
         * - Storage : action de CRUD sur la database
         * - UserManagement : action de gestion de l'enregistrement/connexion des utilisateurs 
         * - UserService : actions accessiblies à l'utilisateur
         *
         * Doté de ces objets Fred peut :
         * - Afficher une interface d'authentification
         * - Afficher une interface de connexion
         * - Afficher une interface de désauthentification
         * - Afficher une interface de déconnexion
         * - Afficher l'interface principale
         * - Recueillir du pseudo code utilisateur
         * - Transférer le pseudocode utilisateur vers le compilateur
         * - Compiler une grammaire
         * - Afficher le résultat
         *
         *
         * */

         //Mise en route de la base de donnée
         new Admin().SetupDatabase();

        // Instanciation de l'utilisateur courant (1 interface == 1 utilisateur)
        USER = new User();

        // Instanciation d'un moteur graphique
        X = new Xcore();

        // Instanciation d'une database
        DB = new Database();

        // Instanciation d'un compilateur
        Compiler = new CompilerEngine(X); // Nous lui passons une instance de X
        Console.Todo("remplacer le passage de X en argument par le passage d'un listener");


        /** 
         * @Tests
         * Pour les recetteurs, Fred dispose de 2 objets supplémentaires dédiés :
         * - aux tests unitaires
         * - aux tests fonctionnels
         * du logiciel 
         *
         * Ainsi que d'une classe Console pour controler les messages de sortie, les todos et 
         * les exceptions 
         * - Console.Log(... s)
         * - Console.Warn(String s)
         * - Console.Err(String s)
         * - Console.Success(String s)
         * - Console.Todo(String s)
         * - Console.Except(Expression e)
         *
         **/


        // Instanciation d'une unité de tests unitaires
        UnitTest = new TestUnit();

        // Instanciation d'une unité de tests fonctionnels
        UseTest = new UseCase();


        /** 
         * @Test
         * Les tests sont appelés en invoquant Fred avec l'argument "test"
         * 
         **/


        // Démarrage des tests
        if (args.length > 0 && args[0].equals("test")) {
            UseTest.StartRequirements();
            UnitTest.TestAll();
            Console.Success(null);
            return;
        }


        /** 
         * @Fonctionnement
         * Lorsque Fred est lançé AVEC 1 argument, et si cet argument n'est pas "test", 
         * Fred délègue la gestion de cet argument à sa classe parent CompilerEngine.
         *
         * */


        else if (args.length > 0) {

            Compiler.ParseFileOrFolder(args[0]);

        }


        /** 
         * @Fonctionnement
         * Lorsque Fred est lançé SANS argument il construit des éléments d'interface 
         * d'enregistrement et d'authentification puis les passe en arguments à X 
         * afin de recueillir les données personnelle de l'utilisateur.
         *
         */


        else {


            /** 
             * @Fonctionnement
             *
             * ## Construction des éléments d'interface graphique
             * 
             * Les éléments d'interface graphique dont Fred dispose sont de deux types:
             * - D'une part les "Components", qui sont des éléments graphique 
             *   unitaires (boutons, aires de texte, champ utilisateur, etc.), sont situés 
             *   dans le package View.Components
             * - D'autre part les "Pages", qui organisent les Components en vues fonctionnelles 
             *   manipulables par Xcore, sont situées dans le package View
             *
             * L'objectif de Fred est de fournir ces objets à Xcore afin que ce dernier 
             * puisse les afficher.
             *
             * Voici les variables qui contiendront les Pages de l'application:
             * - mx_built : Page principale de l'application
             * - Login login_built : Page d'authentification
             * - Register register_built : Page d'inscription
             * - Success success_built : Page de compilation réussie
             * - Failure failure_built : Page de compilation échouée
             * - Userboard ub_built : Configuration des données de l'utilisateur
             *
             * Chaque Page est constituée de Components dont certains seront utilisé pour détecter 
             * le comportement de l'utilisateurs et d'autres sont utilisés pour stocker des données
             * personnelles
             *
             * Les Components porteurs de données personnelles portent le nom Data.*
             *
             * Ils doivent être référencés par Fred dès maintenant afin de pouvoir être,
             * ensuite, injectés dans des vues, qui viendront constituer des Page
             * qu'Xcore pourra afficher.
             *
             * En pratique, avant de construire les pages de l'application nous allons 
             * au préalable créer des Components spécifiques, dit "porteurs de données
             * personnelles" ainsi que des Components tout aussi spécifiques mais réalisant, eux, 
             * des actions (comme le boutton Login par exemple). Restera ensuite à passer ces 
             * actions aux constructeurs des différents Components afin d'être inclus dans 
             * l'interface graphique. En conservant ici la référence à ces 
             * objets nous serons plus tard en mesure de les controler et d'en lire le contenu.
             *
             */


            // Components porteurs de données personnelles
            lpass = new JPasswordField(); // Contient le Password à l'écran Login
            lemail  = new JTextField(); // Contient Email à l'écran Login
            rpass = new JPasswordField(); // Contient le Password à l'écran Register
            remail = new JTextField(); // Contient Email à l'écran Register
            rname = new JTextField(); //Contient Name à l'écran register
            code = new JTextArea(); // Contient le Pseudocode utilisateur
            score = new JLabel(USER.Score.toString()); // Contient le Score
            level = new JLabel(USER.Level()); // Contient la représentation du Score
            pass1 = new JPasswordField(); // Contient le Password à l'écran Edition des données
            pass2 = new JPasswordField(); // Contient le nouveau Password
            ubmail = new JTextField(); // Contient le nouveau Email
            ubname = new JTextField(); // Contient le nouveau Name

            // Components réalisant des actions
            login = new JButton("Login"); // Connecte un utilisateur et dirige vers MainX
            toregister = new JButton("Register Form"); // Dirige un utilisateur vers Register
            register = new JButton("Register"); // Enregistre un utilisateur
            tologin = new JButton("Login Form"); // Dirige un utilisateur vers Login
            back1 = new JButton("Back"); // Retour à l'écran Login
            back2 = new JButton("Back"); // Retour à l'écran Register
            eval = new JButton("Evaluer"); // Lance l'évaluation de pseudocode 
            compile = new JButton("Compiler"); // Lance la compilation de pseudocode
            export = new JButton("Exporter"); // Lance l'exportation du pseudocode
            exit = new JButton("Quitter"); // Quitte Fred
            edituser = new JButton("Préférences"); // Ouvre la configuration utilisateur
            canceledit = new JButton("Annuler"); // Annule l'édition utilisateur et revient à MainX
            validedit = new JButton("Valider"); // Valide l'édition utilisateur et revient à MainX

            /**
             * @Fonctionnement
             * Les Components destinés à détecter le comportement utilisateurs 
             * sont spécifiquement rattachées à la méthode ActionListener de Fred,
             * obtenu grâce à l'import de java.awt.event.ActionListener.
             *
             */


            register.addActionListener(this);
            login.addActionListener(this);
            tologin.addActionListener(this);
            toregister.addActionListener(this);
            back1.addActionListener(this);
            back2.addActionListener(this);
            exit.addActionListener(this);
            export.addActionListener(this);
            compile.addActionListener(this);
            eval.addActionListener(this);
            edituser.addActionListener(this);
            canceledit.addActionListener(this);
            validedit.addActionListener(this);
            pass1.addActionListener(this);
            pass2.addActionListener(this);
            ubmail.addActionListener(this);
            ubname.addActionListener(this);


            /**
             * @Fonctionnement
             * Les pages de l'application peuvent à présent être fabriquées à partir des Components
             * créés et initialisés auparavant, passés en argument. Pour des raisons de sécurité
             * nous n'allons consruire à présent que les pages nécessaires à l'authentification et
             * à l'inscription.
             *
             */

            // Construction de la page d'authentification
            login_built = new Login(lpass, lemail, login, toregister);
            login_built.Build();

            // Construction de la page d'inscription
            register_built = new Register(rpass, remail, rname, register, tologin);
            register_built.Build();

            /**
             * @Fonctionnement
             * Reste, pour Fred, à charger ces Pages dans son moteur graphique Xcore. Pour
             * des raisons de sécurité les pages liées à l'inscription et à la connexion sont
             * diffusé (par Xcore) dans un channel spécifique (Auth). Le reste des pages sera
             * diffusé dans un autre channel (App) ce qui devrait rendre impossible l'accès aux
             * pages d'inscription et de connexion une fois authentifié.
             *
             */

            //Paramétrage de Xcore
            X.Channel = "Auth";
            X.Load(login_built, "Login", "Auth");
            X.Load(register_built, "Registration", "Auth");


            /**
             * @Fonctionnement
             * Fred peut à présent démarrer Xcore 
             *
             */
             
            // Démarrage de Xcore sur le channel Auth 
            X.Start("Auth", login_built);

        }
    }

    public static void main(String[] args) {

        Fred fred = new Fred(args);

    }

    /** ............................................... 
     * @Methodes
     * Méthodes relatives à l'interface ActionListener
     ** .............................................. */

    /**
     * @see actionPerformed 
     * @param listener
     * 
     * @see Register
     * @see setText
     * @param Email
     * @see getsource 
     **/

    @Override
    public void actionPerformed(ActionEvent listener) {

        if(listener.getSource() == register) {

            /**
             *
             * @Register
             * L'enregistrement d'un utilisateur nécessite 3 informations:
             * - son nom
             * - son email
             * - son mot de passe
             *
             *   Nous allons récupérer ces informations depuis la vue, puis
             *   tenter d'enregistrer l'utilisateur dans la base de donnée de
             *   Fred.
             *
             */

            // Si tous les champs ne sont pas remplis, nous nous arrêtons là
            if(rname.getText().equals("") || 
                    remail.getText().equals("") || 
                    rpass.getPassword().length == 0) {
                return;
            }

            // Dans le cas inverse nous récupérons les informations divulguées par l'utilisateur
            USER.Name = new Data(rname.getText());
            USER.Email = new DataEmail(remail.getText());
            USER.Password = new DataPass(rpass.getPassword());

            // Tentative d'enregistrement de l'utilisateur
            int res = USER.Register();

            if(res == 0) {

                // En cas de réussite, on adapte le contenu de son affichage afin 
                // de préremplir certaines informations
                lemail.setText(USER.Email.getText());
                lpass.setText(String.valueOf(rpass.getPassword()));

                // Puis l'utilisateur est transféré vers l'écran de Login
                X.Move(login_built);


            // En cas d'échec on en informe l'utilisateur
            } else {

                if (res == 1) {

                    X.WarnUser("L'email fournit est invalide");

                } else if(res == 2) {

                    // On adapte le contenu de l'affichage afin de préremplir certaines
                    // informations
                    lemail.setText(USER.Email.getText());
                    lpass.setText(String.valueOf(rpass.getPassword()));

                    // On déplace l'utilisateur
                    X.Move(login_built);

                    // Et on le prévient
                    X.WarnUser("L'email fournit existe déjà, essayez de vous logger");
                }

            }

        } else if (listener.getSource() == toregister) {

            X.CleanWarnings();
            X.Move(register_built);

        } else if(listener.getSource() == login) {


            /**
             * @Auth
             * L'authentification d'un utilisateur requiert 2 informations:
             * - son email
             * - son mot de passe
             *
             * Nous allons récupérer ces informations depuis la vue, puis
             * tenter de connecter l'utilisateur à Fred
             * 
             * Note : L'algorithme d'authentification est basé sur un mot de passe qui n'est 
             * pas stocké par le controlleur. La seule référence au mot de passe en clair 
             * réside dans la vue.
             *
             */


            // On enregistre l'email de l'utilisateur (mais pas son mot de passe)
            USER.Email = new DataEmail(lemail.getText());

            // On tente de logguer l'utilisateur
            int res = USER.Login(lpass.getPassword());

            // En cas de réussite, l'utilisateur est transféré vers l'application principale
            if (res == 0) {

                /**
                 *
                 * @Database
                 * Dès lors que le mot de passe d'un utilisateur est valide, la connexion à la 
                 * base de donnée Sqlite n'a plus de raison d'être bloquée.
                 *
                 * Nous sélectionnons toutes les informations de l'utilisateur dont nous détenons 
                 * l'ID.
                 *
                 * Et nous demandons à un administrateur de connecter l'utilisateur
                 *
                 */


                // Un administrateur fabrique pour nous un utilisateur valide
                USER = new Admin().ReadUser(USER.Id); // L'utilisateur est connecté
                

                /**
                 * @Fonctionnement
                 * Par la suite certaines pages auront besoin d'afficher certaines données 
                 * utilisateur. Par exemple le nom et le score, pour la page principale (MainX);
                 * Afin d'éviter une connexion directe entre le Modèle et la Vue, nous avons recours 
                 * à un objet intermédiaire, détenu par le package Control, qui package pour nous
                 * des données froides liées à l'utilisateur.
                 *
                 */


                userdata = new UserData(USER); //Container portant des information pour la Vue


                /**
                 *
                 * @Auth
                 * Nous mettons également à jour le Score et le Level de l'utilisateur, reçu depuis la base
                 * de donnée vers la variable encore à zéro hérité par l'utilisateur du moment
                 * où il n'était pas encore loggué.
                 *
                 */

                score = new JLabel(USER.Score.toString());
                level = new JLabel(USER.Level());

                /**
                 *
                 * @Auth
                 * Nous construisons maintenant les pages de l'application
                 *
                 */


                // Construction de la page principale
                mx_built = new MainX(code, level, score, 
                        eval, compile, export, exit, edituser, userdata);// UserData est içi
                mx_built.Build();

                // Construction de la page de compilation réussie
                success_built = new Success(back1);
                success_built.Build();

                // Construction de la page de compilation échouée
                failure_built = new Failure(back2);
                failure_built.Build();

                // Construction de la page de gestion des données personnelles de l'utilisateur
                ub_built = new Userboard(pass1, pass2, 
                        ubmail, ubname, userdata, canceledit, validedit); // UserData est içi
                ub_built.Build();


                /**
                 *
                 * @Auth
                 * Puis nous les chargeons dans le moteur graphique
                 *
                 */


                X.Load(mx_built, "MainX", "App");
                X.Load(success_built, "Success", "App");
                X.Load(failure_built, "Failure", "App");
                X.Load(ub_built, "Settings", "App");

                // Nous sélectionnons le bon Channel de diffusion
                X.Channel = "App";

                // Et affichons le résultat
                X.Move(mx_built);

            } 

            // En cas d'échec on en informe l'utilisateur
            else {

                if (res == 1) {
                    X.WarnUser("L'email fournit est invalide");
                }

                else if (res == 2) {
                    remail.setText(USER.Email.getText());
                    X.WarnUser("L'email fournit n'existe pas encore");
                }

                else if (res == 3) {
                    X.WarnUser("Le mot de passe est invalide");
                }
            }

        } else if (listener.getSource() == tologin) {

            X.CleanWarnings();
            X.Move(login_built);

        } else if (listener.getSource() == export) {

            Console.Todo("Start_export (td)");

        } else if (listener.getSource() == back1) {

            X.Move(mx_built);

        } else if (listener.getSource() == back2) {

            X.Move(mx_built);

        } else if(listener.getSource() == edituser) {

            X.Move(ub_built);

        } else if(listener.getSource() == canceledit) {

            X.Move(mx_built);

        } else if(listener.getSource() == validedit) {


            /**
             * @Settings
             * Edition d'un utilisateur
             *
             * Plutôt que de traiter chaque entrée séparement, nous allons copier toutes les
             * données de l'utilisateur dans un objet UserData temporaire, puis fournir 
             * cet objet l'User à une fonction (détenue par Admin).
             *
             * Celle-ci saura détecter les changements et procéder aux mises à jour.
             *
             * Le processus d'édition se déroule en 3 étapes :
             * - Construction de UserData
             * - Copie des nouvelles données de l'utilisateur dans UserData
             * - Intervention d'un administrateur pour traiter UserData
             *
             */


            /**
             *
             * @Settings
             * Construction d'un object UserData
             *
             *
             */


            UserData userdata = new UserData(USER);


            /**
             * @Settings
             * Copie des nouvelles données dans l'objet UserData
             * Sauf le mot de passe, pour des raisons de sécurité
             * UserData n'accepte pas de mot de passe
             *
             */


            userdata.Name = new Data(ubname.getText());
            userdata.Email = new DataEmail(ubmail.getText());


            /**
             * @Settings
             * Traitement à part du changement de mot de passe
             *
             */


            DataPass passold = new DataPass(USER.Password.Salt, pass1.getText());
            DataPass passnew = new DataPass(pass2.getText());


            /**
             * @Settings
             * Intervention d'un administrateur pour mettre à jour l'utilisateur
             *
             */

            Integer res = new Admin().UpdateUser(USER, userdata, passold, passnew);

            /**
             * @Settings
             * On detecte les erreurs
             *
             */

            if(res == 1) {

                X.WarnUser("Le nom fourni n'est pas valide");
                return;
            }

            else if (res == 2) {
                X.WarnUser("Cet email est utilisé par un autre utilisateur");
                return;
            }

            else if (res == 3) {
                X.WarnUser("L'email fourni n'est pas valide");
                return;
            }

            else if (res == 4) {
                X.WarnUser("Le mot de passe actuel est requis et doit être valide");
                return;
            }

            else if(res != 0) {
                X.WarnUser("Erreur non gérée");
                return;
            }


            /**
             * @Settings
             * Si l'opération est un succès, nous :
             * - nous mettons à jour l'objet userdata
             * - nous reconstruisons la page principale en lui passant un userdata à jour
             * - reconstruisons également l'Userboard en prevoyance du retour de l'utilisateur
             * - renvoyons l'utilisateur vers la page principale reconstruite
             *
             */

            // Mise à jour d'userdata
            userdata.Name = USER.Name;
            userdata.Email = USER.Email;

            // Nettoyage intermédiaire
            pass1.setText("");
            pass2.setText("");

            // Reconstruction de Userboard afin d'incorporer les changements
            ub_built = new Userboard(pass1, pass2, 
                    ubmail, ubname, userdata, canceledit, validedit); // UserData est içi
            ub_built.Build();
            X.Load(ub_built, "Settings", "App");

            // Reconstruction de MainX afin d'incorporer les changements
            mx_built = new MainX(code, level, score, 
                    eval, compile, export, exit, edituser, userdata);// UserData est içi
            mx_built.Build();
            X.Load(mx_built, "MainX", "App");

            // On affiche la page principale
            X.Move(mx_built);
            X.UpdateView();
            X.CongratzUser("Modifications prises en compte");

            Console.Todo("Mériterait un refactoring ici");

        } else if(listener.getSource() == exit) {

        System.exit(0);

    } else {

        // PseudoCode processing
        String pseudocode = code.getText();

        if (listener.getSource() == eval) {

            Boolean Check = Compiler.Verify(pseudocode);

            if(Check == true) {

                X.CleanPresentation();
                X.CongratzUser("Eval is right");
                new Admin().IncreaseUserScore(USER, pseudocode.length());

            } else {

                X.WarnUser("Eval is wrong");
                new Admin().DecreaseUserScore(USER, pseudocode.length());
            }

            // Reconstruction de score et level afin d'afficher les changements
            score.setText(USER.Score.toString());
            level.setText(USER.Level());

            // Mise à jour de X
            X.UpdateView();

        } else if (listener.getSource() == compile) {

            Boolean Check = Compiler.Verify(pseudocode);

            if(Check == true) {
                new Admin().IncreaseUserScore(USER, pseudocode.length());
                X.Move(success_built);
            } else {
                new Admin().DecreaseUserScore(USER, pseudocode.length());
                X.Move(failure_built);
            }
        }

    }

    }


    /**
     *
     * @Technical
     * Testé sous i686 GNU/Linux, version du noyau : 4.9.0-9-686-pae
     * openjdk version "1.8.0_222"
     * Ecrit sous Vim 8.0
     *
     */

    /**
     * @FAQ (Foire aux Questions)
     *
     *
     * **Qui est Frédéric Jaquenod?**
     * Un ingénieur au CNAM et professeur d'algorithmie et système Unix 
     * à l'université Paris 1 La Sorbonne, dans le cadre du master 2 Métiers de l'informatique 
     * et maitrise d'ouvrage
     * - [Master 2 Mimo](https://www.pantheonsorbonne.fr/diplomes/mimo/vie-etudiante/master-2-mimo/)
     * - [Administration des réseaux - Frédéric Jacquenod](https://www.amazon.it/Administration-r%C3%A9seaux-Fr%C3%A9d%C3%A9ric-Jacquenod/dp/2744013439)
     *
     *
     * **Comment faire remonter un bug?**
     * En ouvrant un ticket sur le gitlab du projet, dans la rubrique "issues" : 
     * [Participez au développement](https://gitlab.com/guillaume.ferron/jhackno-compiler)
     *
     *
     * **Comment contacter les auteurs de ce logiciel?**
     * En envoyant un email à l'un ou à l'autre des auteurs:
     * - guillaume.ferron@gmail.com
     * - pierre.courtois@ymail.com
     *
     */

}
