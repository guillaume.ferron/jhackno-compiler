package Tests;

import Control.*;
import Exceptions.E_Example;

import javax.swing.*;

public class TestUnit extends Console {

    Xcore X;

    public TestUnit () {
        X = new Xcore();
    }

    public void TestAll() {

        Title("Démarrage des tests unitaires");

        TestX();
    }

    public void TestX() {

        TitleSub("Démarrage des tests unitaire de Xcore");

        XTest1();
    }

    public void XTest1() {
        Todo(null);
    }

    public void TestExample() {

        //Exemple
        Tell("Test if 1 == 2");

        try {
            
            if (1 == 2) {
                Success("Hell yay, 1 ==2...");
            } else {
                throw new E_Example("1 == 2");
            }

        } catch (E_Example e) {

            Except(e, true);
        }

    }
}
