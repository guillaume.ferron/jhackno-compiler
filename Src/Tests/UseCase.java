package Tests;

import java.util.*;
import Control.*;


public class UseCase extends Console {

    ArrayList<String> Requirements;

    public UseCase () {

    }

    public void StartRequirements() {

        Title("Démarrage de l'évaluation fonctionnelle");
        Requirements = new ArrayList<String>();

        Start(1, "Add your requirement here");
        Start(2, "Add your requirement here");
        Todo(null);
    }

    public void Start(Integer index, String required) {

        Tell(required);
        Exec(index);
    }

    public void Exec(Integer index) {
        switch (index) {
            case 1:  u1();
                     break;
            case 2:  u2();
                     break;
        }
    }

    public void u1() {
       Tell("doing something..."); 
    }

    public void u2() {
       Tell("doing something else..."); 
    }

}
