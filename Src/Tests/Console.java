package Tests;

import java.util.*;

import Model.User;
import Model.DataPass;
import Model.Data;
import Control.UserData;

public class Console {

    private static int TODO_STATE = 0;

    public static void Finish(String x) {
        Tell(x);
        System.exit(0);
    }

    public static void Finish() {
        System.exit(0);
    }

    public static void Log(Map<String,Object> x) {
        System.out.println("Map<String,Object> : ");
        System.out.println(x.toString());
    }

    public static void Log(String x) {
        System.out.println("STRING : ");
        System.out.println(x);
    }

    public static void Log(Integer x) {
        System.out.println("INTEGER : ");
        System.out.println(x);
    }

    public static void Log(char[] x) {
        System.out.println("CHAR[] : ");
        System.out.println(x);
    }

    public static void Log(User x) {
        System.out.println("USER : ");
        System.out.println(x.toString());
    }

    public static void Log(DataPass x) {
        System.out.println("DATAPASS : ");
        System.out.println(x.toString());
    }

    public static void Log(UserData x) {
        System.out.println("USERDATA : ");
        System.out.println(x.toString());
    }

    public static void Log(Data x) {
        System.out.println("DATA : ");
        System.out.println(x.toString());
    }

    public static void Err(String x) {
        if (x != null) {
            System.out.println("Error : " + x);
        } else {
            System.out.println("Error ");
        }
    }

    public static void Except(Exception x, Boolean block) {

        if(x == null) {
            Err(null);
        } else {
            //x.printStackTrace();
            System.out.println(x.getMessage());
            if(block == true) {
                Warn("Exception bloquante");
                System.exit(1);
            }
        }

    }

    public static void Shout(String x) {
        if (x == null) {
            String payload = "HEY HO!";
            System.out.println("SHOUT : " + (String) payload);
        } else {
            System.out.println("SHOUT : " + x);
        }
    }

    public static void Warn(String x) {
        if (x == null) {
            System.out.println("Warning ");
        } else {
            System.out.println("Warning : " + x);
        }
    }

    public static void Success(String x) {
        if (x == null) {
            System.out.println("Success ");
        } else {
            System.out.println("Success : " + x);
        }
    }

    public static void Tell(String x) {
        System.out.println("# " + x);
    }

    public static void Title(String x) {
        System.out.println(x.toUpperCase());
    }

    public static void TitleSub(String x) {
        System.out.println("-> " + x.toUpperCase());
    }

    public static void Todo(String x) {

        if(TODO_STATE == 0) return;

        if (x == null) {
            System.out.println("#Todo ");
        } else {
            System.out.println("#Todo : " + x);
        }
    }

}
