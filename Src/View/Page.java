/**
 *
 * @Fabrication d'une Page
 *
 * Une Page est un JPanel qui:
 * - 
 * - Ecoute la fenêtre qui le contient afin d'adapter sa position
 *
 */

// Cette classe est abstraite et appartient au package View
package View;

// Ux lib
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.CardLayout;
import javax.swing.BoxLayout;

// Package Fred
import View.Components.*;
import Interfaces.Layout;
import Tests.*;

public abstract class Page extends JPanel implements Layout {

    /**
     *
     * L'objectif de la classe Page est de fournir un modèle de vue (d'où sa présence dans
     * le package View) correspondant à un écran générique d'application
     *
     */

    //Impacte la taille de la fenêtre
    public Integer Pagesize = 0; //Zero équivaut à plein écran

    // La méthode Build vise à renvoyer un object graphique de type *.Page
    public abstract void Build();

    @Override
    public void setLayout(String layout) {

        switch (layout) {
            case "border":
                super.setLayout(new BorderLayout());
                break;

            case "grid":
                super.setLayout(new GridLayout(2,3));
                break;

            case "box":
            case "boxY":
                super.setLayout (new BoxLayout(this, BoxLayout.Y_AXIS));  
                break;

            case "boxX":
                super.setLayout (new BoxLayout(this, BoxLayout.X_AXIS));  
                break;

            case "flow":
                super.setLayout(new FlowLayout(FlowLayout.LEFT));
                break;

            case "card":
                super.setLayout(new CardLayout());
                break;

            default:
                break;
        }
    }

}

