package View;

// Standard lib
import javax.swing.*;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;

// Fred lib
import View.Components.*;
import Control.UserData;

public class Userboard extends Page {

    public UserData userdata;
    public JPanel ub_built;
    public JButton bouton_ubok;
    public JButton bouton_ubcancel;
    public JPasswordField ubpassword1_text;
    public JPasswordField ubpassword2_text;
    public JTextField ubusername_text;
    public JTextField ubemail_text;
    public JLabel ubemail_label;
    public JLabel ubpassword1_label;
    public JLabel ubpassword2_label;
    public JLabel ubusername_label;
    public JButton bouton_edituser;

    public JPasswordField pass1;
    public JPasswordField pass2;
    public JTextField ubmail;
    public JTextField ubname;


    public Userboard(JPasswordField pass1, JPasswordField pass2,
            JTextField ubmail, JTextField ubname, 
            UserData userdata, JButton ubcancel, JButton ubok) {

        this.ubemail_text = ubmail;
        this.ubusername_text = ubname;
        this.ubpassword1_text = pass1;
        this.ubpassword2_text = pass2;
        this.userdata = userdata;
        this.bouton_ubcancel = ubcancel;
        this.bouton_ubok = ubok;

        Pagesize = 3;
    }

    public void Build() {

        ub_built = new JPanel();
        ubemail_label = new JLabel();
        ubpassword1_label = new JLabel();
        ubpassword2_label = new JLabel();
        ubusername_label = new JLabel();

        JPanel conteneur = new JPanel();
        JPanel data = new JPanel();
        JPanel name = new JPanel();
        JPanel email = new JPanel();
        JPanel password_old = new JPanel();
        JPanel password_new = new JPanel();
        JPanel buttons = new JPanel();

        name.add(ubusername_label);
        name.add(ubusername_text);

        email.add(ubemail_label);
        email.add(ubemail_text);

        password_old.add(ubpassword1_label);
        password_old.add(ubpassword1_text);

        password_new.add(ubpassword2_label);
        password_new.add(ubpassword2_text);

        buttons.add(bouton_ubcancel);
        buttons.add(bouton_ubok);

        data.add(name);
        data.add(email);
        data.add(password_old);
        data.add(password_new);

        ubusername_label.setText("Name");
        ubusername_text.setText(userdata.Name.getText());
        ubemail_label.setText("Email");
        ubemail_text.setText(userdata.Email.getText());

        ubpassword1_label.setText("Current Password");
        ubpassword2_label.setText("Password");
        bouton_ubok.setText("Save");
        bouton_ubcancel.setText("Leave");

        ubusername_text.setPreferredSize(new Dimension(200,30));
        ubemail_text.setPreferredSize(new Dimension(200,30));
        ubpassword1_text.setPreferredSize(new Dimension(200,30));
        ubpassword2_text.setPreferredSize(new Dimension(200,30));

        ubusername_label.setPreferredSize(new Dimension(200,30));
        ubemail_label.setPreferredSize(new Dimension(200,30));
        ubpassword1_label.setPreferredSize(new Dimension(200,30));
        ubpassword2_label.setPreferredSize(new Dimension(200,30));

        name.setLayout(new FlowLayout());
        email.setLayout(new FlowLayout());
        password_old.setLayout(new FlowLayout());
        password_new.setLayout(new FlowLayout());
        data.setLayout(new BoxLayout(data, BoxLayout.PAGE_AXIS));
        buttons.setLayout(new GridLayout());

        conteneur.setLayout(new FlowLayout());
        data.add(buttons);
        conteneur.add(data);

        buttons.setBorder(BorderFactory.createEmptyBorder(45, 5, 5, 5)); 

        ub_built.setLayout(new BorderLayout());
        ub_built.add(conteneur, BorderLayout.CENTER);
        ub_built.setBorder(BorderFactory.createEmptyBorder(245, 5, 5, 5)); 

        add(ub_built);

    }


}
