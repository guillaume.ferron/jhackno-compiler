package View;

// Standard lib
import javax.swing.*;
import java.awt.Font;
import java.awt.Color;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.border.EmptyBorder;

// Fred lib
import Control.Config;
import Control.UserData;
import View.Components.Row;
import View.Components.Layer;
import Tests.*;

public class MainX extends Page {


    /**
     * @Page
     * La Page MainX est la page principale de l'application graphique délivrée par
     * Fred. Elle est constituée :
     * - d'un header contenant:
     *      - le logo de l'application
     *      - un message de bienvenu à destination de l'utilisateur
     *      - le score et le level de l'utilisateur
     * - d'un champs de texte visant à revueillir le pseudocode utilisateur
     * - d'un bouton "Evaluer" qui évalue et corrige le pseudocode
     * - d'un bouton "Compiler" qui compile et execute le pseudocode
     * - d'un bouton "Exporter" qui offre de télécharger le texte évalué et le résultat de la
     *   compilation.
     *
     */


    public Config config; // Peut contenir la configuration de la plateforme
    public UserData userdata; // Contient des données utilisateurs
    public JTextArea code; // Reçoit le pseudocode utilisateur
    public JLabel level; // Contient le level de l'utilisateur
    public JLabel score; // Contient le score de l'utilisateur
    public JButton eval; // Est un bouton qui lance l'évaluation
    public JButton compile; // Est un bouton qui lance la compilation
    public JButton export;  // Est un bouton qui lance l'export
    public JButton exit; // Est un bouton qui quitte le logiciel
    public JButton edituser; // Est un bouton qui ouvre la page de configuration 
                             // des données personnelles

    public MainX(JTextArea code, JLabel level, JLabel score, JButton eval, 
            JButton compile, JButton export, JButton exit, JButton edituser, UserData userdata) {


        /**
         * @Page
         * Comme toutes les Page, MainX reçoit en argument tous les JComponents et Components 
         * référencés par Fred et auxquels ont été affectés un Event Listener.
         *
         * Chaque Page doit récupérer et enregistrer les éléments reçus, puis les utiliser
         * dans son design.
         *
         */


        // Enregistrement des Components reçus
        this.code = code;
        this.level = level;
        this.score = score;
        this.eval = eval;
        this.compile = compile;
        this.export = export;
        this.exit = exit;
        this.edituser = edituser;
        this.userdata = userdata;

        // Les pages peuvent importer la configuration de Fred afin 
        // de disposer d'informations sur le logiciel, en vue de les
        // afficher
        config = new Config();

        // Taille de la fenêtre
        Pagesize = 3;
        Console.Todo("Réparer bug d'affichage du Footer quand PageSize=0");

    }

    public void Build() {


        /**
         *
         * @MainX
         * Nous commençons par créer les container éléments dont nous avons besoin 
         *
         */


        Row Header; // En-tête
        Row Main; // Corps principal
        Row Footer; // Conteneur des bouttons

        Header = new Row("border");
        Main = new Row("border");
        Footer = new Row("grid");


        /**
         *
         * @MainX
         * Puis nous créons les sous-éléments (sauf ceux envoyés par Fred, que nous recyclerons à
         * la place)
         *
         *
         */


        JLabel logo;
        JLabel hello;
        /* Envoyés par Fred :
           JLabel level;
           JLabel score;
           JTextArea code;
           JButton edituser;
           JButton eval;
           JButton compile;
           JButton export;
           JLabel exit;
        */


        logo = new JLabel();
        hello = new JLabel();;
        /* Envoyés par Fred :
           level = new JLabel();
           score = new JLabel();
           code = new JTextArea();
           edituser = new JButton();
           eval = new JButton();
           compile = new JButton();
           export = new JButton();
           exit = new JButton();
           */


        /**
         * @MainX
         * Ensuite, nous configurons les élements qui ont besoin de l'être
         *
         */


        logo.setText(config.GetName());
        logo.setFont(new Font("Courier New", Font.BOLD, 42));

        hello.setText("Hello " + userdata.Name.getText());
        code.setText("Placez votre pseudocode içi");

        score.setFont(new Font("Courier New", Font.BOLD, 12));
        level.setFont(new Font("Courier New", Font.ITALIC, 12));


        /**
         * @MainX
         * Puis nous groupons les élements
         *
         */


        Row a = new Row("border");
        Row b = new Row("grid");
        Row c = new Row("flow");
        Row d = new Row("border");
        JLabel e = new JLabel("Bienvenue sur le compilateur de pseudo-code JACKN0-ISO-2019");
        Row f = new Row("border");
        Row g = new Row("border");
        Row h = new Row("boxX");

        b.add(eval);
        b.add(compile);
        b.add(export);

        g.add(logo, BorderLayout.NORTH);
        g.add(hello, BorderLayout.SOUTH);

        f.add(level, BorderLayout.CENTER);
        f.add(score, BorderLayout.SOUTH);

        c.add(f);

        a.add(e, BorderLayout.WEST);
        a.add(c, BorderLayout.EAST);

        d.add(g, BorderLayout.WEST);

        h.add(edituser);
        h.add(exit);

        Header.add(d, BorderLayout.WEST);
        Header.add(h, BorderLayout.EAST);

        Main.add(a, BorderLayout.NORTH);
        Main.add(code, BorderLayout.CENTER);

        Footer.add(b);


    /**
     *
     * @MainX
     * Doté de ces éléments nous pouvons construire le design de la Page
     *
     */

        Layer layer = new Layer();
        layer.setLayout("border");

        layer.add(Header, BorderLayout.NORTH);
        layer.add(Main, BorderLayout.CENTER);
        layer.add(Footer, BorderLayout.SOUTH);


    /**
     *
     * @MainX
     * Puis ajouter le résultat à la Page en cours
     *
     */

        
        // On fixe un BorderLayout CENTER au layer afin qu'il
        // utilise toute la fenetre disponible.
        setLayout("border");

        // On fixe un peu de padding pour aérer la vue
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5)); 

        // On ajoute le layer à la page en cours
        add(layer, BorderLayout.CENTER);


    }


}
