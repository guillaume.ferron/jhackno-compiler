package View.Components;

import java.awt.Dimension;
import javax.swing.*;
import java.awt.Color;

public class Row extends Layer {

    public Row() {
        setSize(0, 45);
        setLayout("grid");
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    }


    public Row(String layout) {
        setSize(0, 45);
        setLayout(layout);
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    }
}
