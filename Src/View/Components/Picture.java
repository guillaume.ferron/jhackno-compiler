package View.Components;

import javax.imageio.*;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.*;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

// Exceptions
import java.io.IOException;

public class Picture extends JLabel {

    public String author;
    public String licence;
    private JLabel pic;

    public Picture() {}

    public Picture(ImageIcon pic) {

        super(pic);

    }

   public void Display() {
       try {
           JOptionPane.showMessageDialog(null, pic);
       } catch (Exception e) {
           //TODO
       }
   }

   public JLabel Get() {
       return pic;
   }

   public void Import(String url) {

       try {
           BufferedImage img = ImageIO.read(getClass().getResource('/'+url));
           ImageIcon icon = new ImageIcon(img);

           //Resizing
           Image image = icon.getImage();
           Image adapted = image.getScaledInstance(-1, 300,  java.awt.Image.SCALE_SMOOTH);  
           ImageIcon result = new ImageIcon(adapted);  // transform it back

           pic = new JLabel(result);


       } catch(IOException e) {
           System.err.println("Image not found: " + e.getMessage());
       }

   }

}
