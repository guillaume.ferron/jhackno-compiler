package View.Components;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.CardLayout;

import Interfaces.Dimensions;
import Interfaces.Layout;
import Tests.Console;

public class Layer extends JPanel implements Layout {

    @Override
    public void setLayout(String layout) {

        Console.Todo("Dont repeat yourself : 'setLayout' in  View/Components/Layer.java <--> View/Page.java");
        switch (layout) {
            case "border":
                super.setLayout(new BorderLayout());
                break;

            case "grid":
                super.setLayout(new GridLayout());
                break;

            case "box":
            case "boxY":
                super.setLayout (new BoxLayout(this, BoxLayout.Y_AXIS));  
                break;

            case "boxX":
                super.setLayout (new BoxLayout(this, BoxLayout.X_AXIS));  
                break;

            case "flow":
                super.setLayout(new FlowLayout(FlowLayout.LEFT));
                break;

            case "card":
                super.setLayout(new CardLayout());
                break;

            default:
                break;
        }
    }

}
