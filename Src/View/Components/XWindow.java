package View.Components;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.SwingUtilities;

import Interfaces.*;

public class XWindow extends JFrame {

    public XWindow() {

        try {
            UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
