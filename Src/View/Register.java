package View;

// Standard lib
import javax.swing.*;
import java.awt.Dimension;
import java.awt.BorderLayout;
import javax.swing.border.EmptyBorder;
import java.awt.Font;

// Fred lib
import View.Components.*;
import Control.Config;

public class Register extends Page {

    //Event Listeners
    public JPasswordField passwordtxt;
    public JTextField emailtxt;
    public JTextField nametxt;
    public JButton registerbtn;
    public JButton tologinbtn;

    public Register(JPasswordField passwordtxt, 
            JTextField emailtxt,
            JTextField nametxt,
            JButton registerbtn,
            JButton tologinbtn) {

        this.passwordtxt = passwordtxt;
        this.emailtxt = emailtxt;
        this.nametxt = nametxt;
        this.registerbtn = registerbtn;
        this.tologinbtn = tologinbtn;

        Pagesize = 2; //PageSize est hérité de Page

    }

    public void Build() {

        Config config = new Config();

	    setLayout("border"); 
        
        Row header = new Row();
        Row main = new Row();
        Row name = new Row();
        Row login = new Row();
        Row password = new Row();
        Row buttons = new Row();

        JLabel Fred = new JLabel(config.GetName());
        Fred.setFont(new Font(Fred.getName(), Font.BOLD, 42));
        header.add(Fred);

        name.add(new JLabel("Name"));
        name.add(nametxt);

        login.add(new JLabel("Email"));
        login.add(emailtxt);

        password.add(new JLabel("Password"));
        password.add(passwordtxt);

        name.setMaximumSize(new Dimension(400,40));
        login.setMaximumSize(new Dimension(400,40));
        password.setMaximumSize(new Dimension(400,40));

        main.add(name);
        main.add(login);
        main.add(password);
        main.setLayout("box");

        main.setBorder(new EmptyBorder(10, 10, 10, 10));

        buttons.add(tologinbtn);
        buttons.add(registerbtn);

        add(header, BorderLayout.NORTH);
        add(main, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);

    }

}
