package View;

// Standard lib
import javax.swing.*;
import java.awt.Dimension;
import java.awt.BorderLayout;
import javax.swing.border.EmptyBorder;
import java.awt.Font;

// Fred lib
import Tests.Console;
import View.Components.*;
import Control.Config;

public class Login extends Page {

    //Event Listeners
    public JPasswordField passwordtxt;
    public JTextField emailtxt;
    public JButton loginbtn;
    public JButton toregisterbtn;

    public Login(JPasswordField passwordtxt, 
            JTextField emailtxt, 
            JButton loginbtn, 
            JButton toregisterbtn) {

        this.passwordtxt = passwordtxt;
        this.emailtxt = emailtxt;
        this.loginbtn = loginbtn;
        this.toregisterbtn = toregisterbtn;

        Pagesize = 2; //PageSize est hérité de Page
    }

    public void Build() {

        Config config = new Config();

	    setLayout("border"); 
        
        Row header = new Row();
        Row main = new Row();
        Row login = new Row();
        Row password = new Row();
        Row buttons = new Row();

        JLabel Fred = new JLabel(config.GetName());
        Fred.setFont(new Font(Fred.getName(), Font.BOLD, 42));
        header.add(Fred);

        login.add(new JLabel("Email"));
        login.add(emailtxt);

        password.add(new JLabel("Password"));
        password.add(passwordtxt);

        login.setMaximumSize(new Dimension(400,40));
        password.setMaximumSize(new Dimension(400,40));

        main.add(login);
        main.add(password);
        main.setLayout("box");

        main.setBorder(new EmptyBorder(10, 10, 10, 10));

        buttons.add(toregisterbtn);
        buttons.add(loginbtn);

        add(header, BorderLayout.NORTH);
        add(main, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);

    }

}
