package View;

// Standard lib
import javax.swing.*;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;

// Fred lib
import View.Components.*;

public class Failure extends Page {

    public JPanel failure_built;
    public JButton back_button1;

    public Failure(JButton back_button) {

        this.back_button1 = back_button;
    }

    public void Build() {

        JPanel capsule = new JPanel(new GridBagLayout());

        JPanel contain = new JPanel();
        contain.setLayout(new BoxLayout(contain, BoxLayout.PAGE_AXIS));

        Picture Jakn0Pic1 = new Picture();
        Jakn0Pic1.Import("Images/jacquenod-failure.jpg");

        failure_built = new JPanel();
        JLabel failure_label = new JLabel("You failed miserably"); 

        JPanel bb_cont = new JPanel();
        bb_cont.setLayout(new GridLayout());
        bb_cont.add(back_button1);

        //Force size workaround
        Dimension d = new Dimension(-1, 50);
        back_button1.setPreferredSize(d);
        back_button1.setMinimumSize(d);
        back_button1.setMaximumSize(d);

        failure_built.setLayout(new GridLayout());

        contain.add(Jakn0Pic1.Get());
        contain.add(bb_cont);

        capsule.add(contain);

        failure_built.add(capsule);
        failure_built.add(failure_label); 

        add(failure_built);

    }


}

