package View;

// Standard lib
import javax.swing.*;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;

// Fred lib
import View.Components.*;

public class Success extends Page {

    public JPanel success_built;
    public JButton back_button1;

    public Success(JButton back_button) {

        this.back_button1 = back_button;
    }

    public void Build() {

        JPanel capsule = new JPanel(new GridBagLayout());

        JPanel contain = new JPanel();
        contain.setLayout(new BoxLayout(contain, BoxLayout.PAGE_AXIS));

        Picture Jakn0Pic1 = new Picture();
        Jakn0Pic1.Import("Images/jacquenod-success.jpg");

        success_built = new JPanel();
        JLabel success_label = new JLabel("Good Enough! :)"); 

        JPanel bb_cont = new JPanel();
        bb_cont.setLayout(new GridLayout());
        bb_cont.add(back_button1);

        //Force size workaround
        Dimension d = new Dimension(-1, 50);
        back_button1.setPreferredSize(d);
        back_button1.setMinimumSize(d);
        back_button1.setMaximumSize(d);

        success_built.setLayout(new GridLayout());

        contain.add(Jakn0Pic1.Get());
        contain.add(bb_cont);

        capsule.add(contain);

        success_built.add(capsule);
        success_built.add(success_label); 

        add(success_built);

    }


}

