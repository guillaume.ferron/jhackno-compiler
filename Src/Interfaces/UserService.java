package Interfaces;

public interface UserService {

    public int Login(char[] clearpass);
    public int Logout();
    public int Register();
    public int Unregister();
    public int Update(String key, String value);

}
