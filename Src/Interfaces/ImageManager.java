package Interfaces;

import javax.swing.*;

/*
* FRED, a Jakn0 compiler
*/

public interface ImageManager {

  public JLabel ImageFromFile(String url);

}
