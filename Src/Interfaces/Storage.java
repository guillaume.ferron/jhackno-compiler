package Interfaces;

/*
* FRED, a Jakn0 compiler
*/
//package Fred.Database;

import java.sql.*;
import java.util.*;

import Model.*;


public interface Storage {

  public void exec(String sql);

  public List<Map<String, Object>> request(String sql);

  public Connection ConnectDb();


}
