package Interfaces;

import java.util.*;

public interface Encryption {

    public HashMap<String, String> GetPassword();
    public Boolean Equals(String clear);
    public HashMap<String, String> EncryptSalted(String clear, String salt);
    public void Encrypt(String clear);

}
