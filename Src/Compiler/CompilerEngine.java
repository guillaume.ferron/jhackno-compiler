package Compiler;

/*
* FRED, a Jakn0 compiler
*/

// Standard lib
import java.util.*;
import java.nio.file.*;
import java.nio.charset.*;
import java.io.*;

// Fred lib
import Compiler.Jakn0.*;
import Tests.Console;
import Control.Xcore;
 
// Antlr4
import org.antlr.v4.runtime.misc.ParseCancellationException;


public class CompilerEngine extends Jakn0 implements Input, Output {

    /**
     *
     * @Compiler
     * La classe CompilerEngine controle la librairie ANTLRv4, à travers une classe intermédiaire 
     * JAKN0. Elle est livrée avec une version zippée d'ANTLR4, sous forme de .jar. La classe 
     * s'occupe d'analyser le pseudocode utilisateur et de délivrer des erreurs dans l'objet 
     * Warning du serveur X.
     *
     *
     */

    
    // Version implémentée d'ANTLR
    public static String version = "4.7.2";

    public Xcore XOutput;

    public CompilerEngine(Xcore X) {
        XOutput = X;
    }

    public Boolean Verify (String Code) {

        /**
         * @Compiler
         * La Methode transfert le code reçu en argument à une 
         * Méthode voisine (Parse) puis retourne un boolean en
         * fonction de la reussite de Parse.
         *
         */

        try {
            Parse(Code);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public void Parse(String Code) {

        /**
         * @Compiler
         * La Methode transfert le code reçu en argument à sa classe 
         * parent (JAKN0), qui est directement connecté à ANTLR4 
         * Elle retourne un boolean en fonction de la reussite de JAKN0.
         *
         */

        try {
            
            //get input
            getInput(Code);
            //input = CharStreams.fromString(Code+"\n");

            //build lexer
            buildLexer();

            //build tokens stream
            buildTokensStream();

            //build parser
            buildParser();

            //build tree
            buildTree();

            //build tree
            displayTree();

        } catch(Exception e) {

            XOutput.TellUser(e.getMessage(), false);
            throw e;

        }

    }

    public void Compile() {

        CustomJakn0Visitor visitor = new CustomJakn0Visitor();
        visitor.visit(tree);

    }

    public void ParseFileOrFolder(String req) {

        String state = "";

        try {
            //Trying to process the path as a file path
            Path path = Paths.get(req);
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for (String line : lines) {
                try {
                    //Read the file line by line
                    Parse(line);
                    state = "success";
                } catch (ParseCancellationException err1){
                    state = "failure";
                } catch (Exception e){
                    Console.Except(e, false);
                } finally {
                    //System.out.printf("%s : %s\n", line, state);
                }
            }

        //Exception, it was not a file
        } catch (IOException e) {
            try {
                //Process it as a String to parse
                Parse(req.toString());
                state = "success";
            } catch (ParseCancellationException err1){
                state = "failure";
            } catch (Exception el){
                Console.Except(el, false);
            } finally {
                //System.out.printf("%s : %s\n", req.toString(), state);
            }
        }
    }

}
