lexer grammar Jakn0Lexers;

ID : [a-zA-Z_]+;
INT : [0-9]+;
NEWLINE : '\r'? '\n';
WS : [ \t]+ -> skip;
