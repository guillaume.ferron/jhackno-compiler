grammar Jakn0;

import Jakn0Lexers;

prog : stat+;

stat : expr NEWLINE                 #code
    | ID '=' expr NEWLINE           #assign
    | NEWLINE                       #blank
    | CLEAR                         #clear
    ;

expr : expr op=('*'|'/') expr       #operation1
    | expr op=('+'|'-') expr        #operation2
    | INT                           #int
    | ID                            #id
    | '(' expr ')'                  #parentheses
    ;

MUL : '*';
DIV : '/';
ADD : '+';
SUB : '-';
CLEAR : 'clear';
