package Compiler.Jakn0;

import java.util.HashMap;
import java.util.Map;

public class CustomJakn0Visitor extends Jakn0BaseVisitor<Integer> {

    Map<String, Integer> memory = new HashMap<String, Integer>();

    @Override
    public Integer visitAssign(Jakn0Parser.AssignContext ctx) {
        String id = ctx.ID().getText();
        int value = visit(ctx.expr());
        memory.put(id, value);
        return value;
    }

    @Override
    public Integer visitCode(Jakn0Parser.CodeContext ctx) {
        Integer value = visit(ctx.expr());
        System.out.println(value);
        return 0;
    }

    @Override
    public Integer visitInt(Jakn0Parser.IntContext ctx) {
        return Integer.valueOf(ctx.INT().getText());
    }

    @Override
    public Integer visitId(Jakn0Parser.IdContext ctx) {
        String id = ctx.ID().getText();
        if (memory.containsKey(id)) return memory.get(id);
        return 0;
    }


    @Override
    public Integer visitOperation1(Jakn0Parser.Operation1Context ctx) {
        int left = visit(ctx.expr(0));
        int right = visit(ctx.expr(1));
        if (ctx.op.getType() == Jakn0Parser.MUL) return left * right;
        return left / right;
    }

    @Override
    public Integer visitOperation2(Jakn0Parser.Operation2Context ctx) {
        int left = visit(ctx.expr(0));
        int right = visit(ctx.expr(1));
        if (ctx.op.getType() == Jakn0Parser.SUB) return left - right;
        return left + right;
    }


    @Override
    public Integer visitParentheses(Jakn0Parser.ParenthesesContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Integer visitClear(Jakn0Parser.ClearContext ctx) {
        memory = new HashMap<String, Integer>();
        return 0;
    }
}
