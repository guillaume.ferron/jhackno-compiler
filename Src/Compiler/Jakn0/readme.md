# How to use?

- Edit the file ./testfile
- To launch the program => ./clean.sh && ./compile.sh && ./run.sh
- To clean .class and ANTLR automatically built files => ./clean.sh

## Files
*source-code refers to "Source Code of your own"*
- testfile => custom source-code 
- Jakn0.g4 => grammar declaration and parsers for source-code
- Jakn0Lexers.g4  => separated lexers for source-code
- Jakn0.java => main class
- CustomJakn0Visitor.java => visitor for parser
- clean.sh => reset example
- compile.sh => compile example
- run.sh => run example
