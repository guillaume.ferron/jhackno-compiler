
package Compiler.Jakn0;
// Generated from Jakn0.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link Jakn0Parser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface Jakn0Visitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link Jakn0Parser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(Jakn0Parser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by the {@code code}
	 * labeled alternative in {@link Jakn0Parser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCode(Jakn0Parser.CodeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assign}
	 * labeled alternative in {@link Jakn0Parser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign(Jakn0Parser.AssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blank}
	 * labeled alternative in {@link Jakn0Parser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlank(Jakn0Parser.BlankContext ctx);
	/**
	 * Visit a parse tree produced by the {@code clear}
	 * labeled alternative in {@link Jakn0Parser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClear(Jakn0Parser.ClearContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link Jakn0Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParentheses(Jakn0Parser.ParenthesesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code operation2}
	 * labeled alternative in {@link Jakn0Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperation2(Jakn0Parser.Operation2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code operation1}
	 * labeled alternative in {@link Jakn0Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperation1(Jakn0Parser.Operation1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code id}
	 * labeled alternative in {@link Jakn0Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(Jakn0Parser.IdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code int}
	 * labeled alternative in {@link Jakn0Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt(Jakn0Parser.IntContext ctx);
}
