#!/bin/bash

cd ..
rm Jakn0Visitor*.java Jakn0Lexer*.java Jakn0Parser*.java Jakn0BaseVisitor*.java > /dev/null  2>&1
rm Jakn0*Listener.java > /dev/null 2>&1
rm *.interp > /dev/null  2>&1
rm *.tokens > /dev/null  2>&1
rm *.class > /dev/null  2>&1
