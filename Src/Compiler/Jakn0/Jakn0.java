
package Compiler.Jakn0;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.FileInputStream;
import java.io.InputStream;

public class Jakn0 {

    public CharStream input = null;
    public Jakn0Lexer lexer = null;
    public CommonTokenStream tokens = null;
    public Jakn0Parser parser = null;
    public ParseTree tree = null;

    public void getInput(String Code) {
        input = CharStreams.fromString(Code+"\n");
    }

    public void buildLexer() {
        lexer = new Jakn0Lexer(input);
    }

    public void buildTokensStream() {
        tokens = new CommonTokenStream(lexer);
    }

    public void buildParser() {
        parser = new Jakn0Parser(tokens);
        parser.addErrorListener(ThrowingErrorListener.INSTANCE);
    }

    public void buildTree() {
        tree = parser.prog();
    }

    public void displayTree() {
        System.out.println(tree.toStringTree(parser));
    }

    public void Visit(String Code) {

        //get input
        getInput(Code);
        //input = CharStreams.fromString(Code+"\n");

        //build lexer
        buildLexer();

        //build tokens stream
        buildTokensStream();

        //build parser
        buildParser();

        //build tree
        buildTree();

        //visit
        CustomJakn0Visitor visitor = new CustomJakn0Visitor();
        visitor.visit(tree);
    }

    public static void main(String[] args) {

        Jakn0 J = new Jakn0();
        J.Visit(args[0]);

    }

}

