package Exceptions;

import java.lang.Exception;

public class E_Example extends Exception {

    public E_Example(String message) {
        super(message);
    }

}
