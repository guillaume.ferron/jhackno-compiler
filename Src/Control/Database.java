package Control;

import java.sql.*;
import java.util.*;

import Interfaces.*;
import Tests.*;

public class Database implements Storage {

    private Connection c;

    public Connection ConnectDb() {

        try {

            Class.forName("org.sqlite.JDBC"); 
            c = DriverManager.getConnection("jdbc:sqlite:Fred.db");
        } catch (Exception e) {
            Console.Except(e, true);
        }

        return c; 
    }

    public void exec(String sql) { //INSERT, UPDATE, DELETE

        ConnectDb();

        try {
            PreparedStatement statement = c.prepareStatement(sql);
            statement.executeUpdate();
        } catch ( Exception e ) {
            Console.Except(e, false);
        }

        try {
            CloseDb();
        } catch (SQLException e) {
            Console.Except(e, true);
        }

    }

    public List<Map<String, Object>> request(String sql) {

        ConnectDb();

        List<Map<String, Object>> resultList = 
            new ArrayList<Map<String, Object>>();
        Map<String, Object> row = null;

        try {
            PreparedStatement statement = c.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();
            Integer columnCount = metaData.getColumnCount();
            while (rs.next()) {
                row = new HashMap<String, Object>();
                for (int i = 1; i <= columnCount; i++) {
                    row.put(metaData.getColumnName(i), rs.getObject(i));
                }
                resultList.add(row);
            }

        } catch ( Exception e ) {
            Console.Except(e, true);
        }

        try {
            CloseDb();
        } catch (SQLException e) {
            Console.Except(e, false);
        }

        return resultList;

    }

    private void CloseDb() throws SQLException {
            c.close();
    }

}
