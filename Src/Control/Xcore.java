package Control;

/*
 * Fred, a Jakn0 compiler
 */

// Project lib
import Compiler.Jakn0.*;
import Compiler.*;
import View.*;
import Model.*;
import View.Components.*;
import Tests.*;

// UI/UX lib
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.plaf.*;
import javax.swing.border.*;
import javax.imageio.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.BorderFactory;

// UI/UX lib Exceptions
import javax.management.BadAttributeValueExpException;

// Standard lib
import java.io.*;
import java.util.*;


/**
 * Xcore combine un système d'authentification (utilisant Database), présenté de manière graphique à l'utilisateur grâce à l'héritage de la classe PageSystem, une interface graphique d'édition des données personnelles de l'utilisateur connecté ainsi qu'une interface graphique contrôlant un objet CompilerEngine.
 * */
public class Xcore extends XWindow {

    // Modèle
    User USER;

    // Components
    public Layer CurrentView;
    public Row Warning;
    public Row Presentation;

    //Navigation
    public String Channel;
    public ArrayList<Screen> ViewStack;

    public Xcore() {

        /**
         * En tant que moteur graphique de l'application, Xcore est également responsable
         * de l'affichage du contenu principal et des notifications. La classe instancie l'objet
         * X, un objet Warning ainsi qu'un objet Presentation, pour assumer ces responsabilités.
         */

        //Création d'un espace de notification sur une ligne
        Warning = new Row("box");

        //Création d'un espace de notification multiligne
        Presentation = new Row("box");

        //Initialisation de la pile de vues
        ViewStack = new ArrayList<Screen>();

        Tests.Console.Todo("Résoudre : 'Console is ambigous' dans Xcore");

    }

    public void Load(Page page, String title, String channel) {

        Screen s = new Screen();

        s.page = page;
        s.title = title;
        s.channel = channel;

        ViewStack.add(s);

    }

    public void Move(Page page) {

        /*
         * Route une requête de Page
         *
         * */

        for (Screen s : ViewStack) {
            if(s.channel.equals(Channel) && s.page.equals(page)) {
                Display(s);
                break;
            }
            Tests.Console.Todo("Else : Display No Page Found error");
        }

    }

    public void Start(String namespace, Page page) {

        /**
         * Démarrage d'Xcore
         @see Start
         @see Move
         @param login_built
         * Lorsque Xcore est lancé, il déplace l'utilisateur vers la fenêtre de connexion.
         * */


        Move(page);
        Tests.Console.Todo("Affichage de register SI aucun utilisateur reconnu en base");

    }

    public void UpdateView() {

        /**
        @see UpdateView
         * Raffraichissement de la fenetre.
         * */

        revalidate();
        repaint();

    }

    public void Display(Screen x) {
        
    /** Affichage de la fenêtre 
        @see Display
        @param x
        @param title
        @param dimension
    */


        Dimension d;

        Channel = x.channel;

        CurrentView = new Layer();
        CurrentView.setLayout("border");
        CurrentView.add(x.page, BorderLayout.CENTER); 

        CleanWarnings();
        CleanPresentation();

        CurrentView.add(Warning, BorderLayout.SOUTH); 

        JScrollPane a = new JScrollPane(Presentation);
        a.setBorder(BorderFactory.createEmptyBorder());
        CurrentView.add(a, BorderLayout.WEST);

        switch (x.page.Pagesize) {

            case 1:
                d = new Dimension(300,100);
                break;

            case 2:
                d = new Dimension(600,300);
                break;

            case 3:
                d = new Dimension(800,600);
                break;

            default:
                d = Toolkit.getDefaultToolkit().getScreenSize();
                break;

        }

        setSize(d);

        setTitle(x.title);
        setContentPane(CurrentView);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(true);

        setVisible(true);  

    }

    public void CleanWarnings() {

        Warning.removeAll();
        revalidate();
        repaint();

    }

    public void CleanPresentation() {

        Presentation.removeAll();
        revalidate();
        repaint();

    }

    public void CongratzUser(String w) {

        CleanWarnings();
        JLabel item = new JLabel(w);
        item.setForeground(Color.GREEN);
        Warning.add(item);
        UpdateView();

    }

    public void WarnUser(String w) {

        CleanWarnings();
        JLabel item = new JLabel(w);
        item.setForeground(Color.RED);
        Warning.add(item);
        UpdateView();

    }

    public void TellUser(String w, Boolean good) {

        JLabel b = new JLabel(w);

        if(good == true) {
            b.setForeground(Color.GREEN);
        } else {
            b.setForeground(Color.RED);
        }

        b.setBorder(new EmptyBorder(0, 5, 0, 20));

        Presentation.add(b, 0);
        UpdateView();
    }

}
