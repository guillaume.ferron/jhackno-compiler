package Control;

// Standard lib
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.text.SimpleDateFormat;

// Fred lib
import Tests.Console;

public class RGPDReference {

    public RGPDReference(String message) {

        Writer writer = null;
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").
            format(Calendar.getInstance().getTime());

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("rgpd.txt"), "utf-8"));
            writer.write("Writed - " + timestamp);
            writer.write(message);
            writer.write("\n");
        } catch (IOException ex) {
            Console.Todo("Report");
        } finally {
            Console.Todo("try {writer.close();} catch (Exception ex) {/*ignore*/}");
        }

    }
}
