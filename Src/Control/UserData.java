package Control;

import Model.User;
import Model.Data;
import Model.DataEmail;
import Model.DataScore;

public class UserData {

    public Data Name;
    public DataEmail Email;
    public DataScore Score;

    public UserData(User u) {
        Name = new Data(u.Name.getText());
        Email = new DataEmail(u.Email.getText());
        Score = new DataScore(u.Score.getInt());
    }

    public String toString() {

        String a = "";
        a += "Name : "+Name.getText()+"\n";
        a += "Email : "+Email.getText()+"\n";
        a += "Score : "+Score.getInt()+"\n";
        a += "\n";
        a += "\n";
        a += "\n";
        return a;
    }

}
