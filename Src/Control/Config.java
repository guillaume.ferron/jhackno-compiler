package Control;

import Model.Meta;

public class Config {

    private Meta m;

    public Config() {
        m = new Meta();
    }

    public String GetName() {
        return m.Name;
    }

    public String GetWelcome() {
        return m.Welcome;
    }

}
