package Model;

public class DataScore extends Data {

    Integer value = 0;

    public DataScore(Integer x) {

        if(x > 0) {
            value = x;
        } else {
            value = 0;
        }

    }

    public String toString() {
        return "Score : " + String.valueOf(value);
    }

    public String getText() {
        return String.valueOf(value);
    }

    public Integer getInt() {
        return value;
        //return Integer.parseInt(getText());
    }

}
