/*
 * FRED, a Jakn0 compiler
 */
package Model;

// Standard lib
import java.security.*;
import java.security.spec.*;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;

// Fred Lib
import Interfaces.*;
import Tests.*;
import Control.Database;

/**
 *
 * La classe User représente un utilisateur de Fred
 * Les utilisateurs sont enregistrés dans une base de donnée Sqlite embarquée par
 * Fred.
 *
 *
 * */

public class User extends Database implements UserService {

    /**
     *
     * Grâce à l'implémentation d'UserService la classe User offre à l'utilisateur la 
     * capacité de gérer
     * - l'insciption
     * - la désinscription
     * - la connexion
     * - la déconnexion
     * - la mise à jour des informations personnelles
     *
     * Cette classe possède trois méthodes propres :
     * - hasNewEmail : test si le mail de l'utilisateur est connu de Fred
     * - Level : attribue à  l'utilisateur une chaine de caractère en fonction de son score
     *
     * */

    public Integer Id;
    public Data Name;
    public DataEmail Email;
    public DataPass Password;
    public DataScore Score;

    public User() {

        Id = -1;
        Name = new Data();
        Email = new DataEmail();
        Password = new DataPass();
        Score = new DataScore(0);

    }

    /**
     * Représentation textuelle de l'utilisateur
     * */

    @Override
    public String toString() {

        String res = "";

        res += "\nId : " + Id + "\n";
        res += "Name : " + Name.getText() + "\n";
        res += "Email : " + Email.getText() + "\n";
        res += "Salt : " + Password.Salt + "\n";
        res += "Hashed : " + Password.Hashed + "\n";
        res += "Score : " + Score.getInt() + "\n";

        return res;

    }

    /**
     * Déconnexion d'un utilisateur
     * */

    public int Logout() {

        Id = -1;
        Name = new Data();
        Email = new DataEmail();
        Password = new DataPass();
        Score = new DataScore(0);

        return 0;

    }

    /**
     * Enregistrement d'un utilisateur
     * */

    public int Register() {

        if (!Email.isValidEmail()) {
            return 1;
        }

        else if(!hasNewEmail()) {
            return 2;
        }

        else {

            String sql1 = "INSERT INTO Users ("
                + "name, "
                + "email, "
                + "salt, "
                + "password"
                +") VALUES ("
                + "'" + Name.getText() + "', "
                + "'" + Email.getText() + "', "
                + "'" + Password.Salt + "', "
                + "'" + Password.Hashed + "'"
                +");";

            exec(sql1);

            String sql = "SELECT count(Id) FROM Users";
            Map<String, Object> m = request(sql).get(0);

            try {
                Id = (Integer) m.get("count(Id)");
            } catch(Exception e) {
                Console.Warn("Cannot set UserID");
                Console.Except(e, false);
            }

            String sql2 = "INSERT INTO Scores ("
                + "userId, "
                + "value "
                +") VALUES ("
                + Id + ", "
                + Score.getInt()
                +");";

            exec(sql2);

            return 0;

        }
    }

    /**
     * Connexion d'un utilisateur
     * */

    public int Login(char[] clearpass) {


        /**
         * @Auth
         * La méthode Login vise à authentifier un utilisateur.
         * Pour ce faire, la classe vérifie la qualité de l'email fourni,
         * vérifie sa nouveauté, et décide lorsque la valeur est neuve et valide,
         * de traiter l'authentification de l'utilisateur rattaché à l'email
         *
         */


        /**
         * 1. On s'assure que l'Email entré par l'utilisateur est valide
         *
         */


        if( ! Email.isValidEmail()) {
            return 1;
        }


        /**
         * 3. On s'assure que l'Email n'est pas nouveau
         *
         */


        if( !! hasNewEmail()) {
            return 2;
        }


        /**
         *
         * @Auth
         * Nous allons maintenant tenter d'authentifier l'utilisateur
         *
         * Pour cela nous allons nous baser sur deux élements qui constituent
         * un mot de passe, à savoir :
         * - le Salt : donnée arbitraire placée en début de mot de passe afin de gonfler la 
         *   complexité en vue de compliquer la tâche d'un attaquant.
         * - le Hash : donnée encryptée qui correspond au résultat de l'opération:
         *   ([Salt] + [Mot de passe en clair]) * Encryption
         *
         * Le Hash et le Salt sont deux valeurs fabriquées par la fonction EncryptSalted à 
         * partir d'un mot de passe en clair
         *
         * EncryptSalted renvoi toujours deux valeurs : le salt utilisé pour l'encryption, et
         * la valeur chiffrée du mot de passe + salt
         *
         */ 


        /**
         *
         * 4. D'abord nous allons chercher les données de l'utilisateur en base grâce à son email
         *
         */


        Map<String, Object> usermap = new Admin().SearchUser(Email.getText());


        /**
         * @Auth
         * Si tout se déroule normalement, nous devrions maintenant disposer d'un HashMap 
         * contenant les différentes informations réceptionnées.
         *
         */


        String salt = (String) usermap.get("Salt");
        String hash = (String) usermap.get("Password");


        /**
         *
         * 5. Ensuite, nous créons un DataPass valide avec les données reçues
         *
         */

        DataPass reference = new DataPass();
        reference.Salt = salt;
        reference.Hashed = hash;


        /**
         *
         * 6. Puis nous créons un DataPass utilisateur avec le Salt reçu et le mail de l'utilisateur
         *
         */


        DataPass verif = new DataPass(reference.Salt, clearpass);
        clearpass = null;


        /**
         *
         * 7. Enfin, nous comparons les deux DataPass
         *
         */


        Boolean verified = reference.Equals(verif);

        if(verified == true) {

            // Si la vérification aboutie, on charge l'utilisateur avec un ID valide
            Id = (Integer) usermap.get("ID");

            // Puis on renvoie 0
            return 0;

        } else {

            //Dans le cas inverse on renvoit 1
            return 3;
        }

    }

    public int Unregister() {

        /**
         * Désenregistrement d'un utilisateur
         * */

        Console.Todo("Unregister!");
        return 0;

    }


    public int Update(String key, String value) {

        if(key.toLowerCase().equals("Score".toLowerCase())) {
            return 1;
        }

        if(key.toLowerCase().equals("Password".toLowerCase())) {

            String sql = "UPDATE Users\n"
                + "SET Salt = "+Password.Salt+",\n"
                + "Password = "+Password.Hashed+"\n"
                + "WHERE Id = "+Id+";";

        } else {

            String sql = "UPDATE Users\n"
                + "SET "+key+" = "+value+"\n"
                + "WHERE Id = "+Id+";";
        }

        return 0;
    }

    public Boolean hasNewEmail() {

        String sql = "SELECT * FROM Users WHERE Email = '"+Email.getText()+"';";
        List r = request(sql);

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            map = (HashMap) r.get(0);
            return false;
        } catch (IndexOutOfBoundsException e) {
            return true;
        } catch (Exception e) {
            Console.Except(e, false);
            return false;
        }

    }

    public String Level() {

        Integer n = Score.getInt();

        if(n == 0) {
            return "Level : Sucker";
        } else if (n <= 10) {
            return "Level : Wannabe";
        } else if (n <= 50) {
            return "Level : Noob";
        } else if (n <= 100) {
            return "Level : Brave Fighter";
        } else if (n <= 250) {
            return "Level : Among the smartest";
        } else if (n <= 500) {
            return "Level : Genious";
        } else if (n < 1000) {
            return "Level : God";
        } else if (n > 999) {
            return "Vous êtes Frederic Jacquenod";
        }

        return "You hacked so much this software";
    }
}
