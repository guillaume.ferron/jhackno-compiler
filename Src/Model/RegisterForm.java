package Model;

import Tests.Console;

public class RegisterForm {

    public Data Name;
    public DataEmail Email;
    public DataPass Password;

    public RegisterForm() {
        Name = new Data(); 
        Email = new DataEmail(); 
        Password = new DataPass(); 
    }

    public User GetObject() {

        Console.Todo("Connecter name, email et password aux events listeners");

        User user = new User();
        user.Name = Name;
        user.Email = Email;
        user.Password = Password;
        return user;
    }

}
