package Model;

import java.io.*;

import Tests.*;
import Control.*;
import Exceptions.*;
import Interfaces.PersonalData;
import Interfaces.SecData;

public class Data implements PersonalData, SecData {

    String value;

    public Data(String s) {

        setText(s);
        Sanitize();

    }

    public Data() {

        getText();

    }

    public String getText() {
        return value;
    }

    public String toString() {
        String a = "";
        a += "Data : "+getText()+"\n";
        return a;
    }

    public void Sanitize() {
        String text = value;
        value = text.replaceAll("[^a-zA-Z0-9@.]+","");
    }

    public void setText(String v) {
        if( value != null //Fix
                && ! value.equals(v)) {
            Reference();
        }
        value = v;
        Tests.Console.Todo("revoir le fix value != null");
    }

    public void Reference() {
        RGPDReference r = new RGPDReference("ReferenceResult : Sensitive data has been created");
    }

}
