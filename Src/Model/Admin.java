package Model;

/*
 * FRED, a Jakn0 compiler
 */

// Standard lib
import java.util.*;

// Fred lib
import Interfaces.*;
import Control.*;
import Tests.*;

public class Admin extends User implements DatabaseSetup, UserManagement {

    /**
     *
     * La classe Admin effectue toutes les actions sensibles pour Fred.
     *
     */

    Database DB;

    public Admin() {

        Id = 0;
        Name = new Data("Admin");
        Email = new DataEmail("a@b.cr");

        DB = new Database();
        SetupDatabase();

    }

    public Integer UpdateUser(User USER, UserData userdata, 
            DataPass oldpass, DataPass newpass) {

        /**
         *
         * @User
         * La Mise à jour d'un utilisateur consiste à remplacer en base 3 valeurs
         * - le nom de l'utilisateur
         * - l'email de l'utilisateur
         * - le mot de passe de l'utilisateur
         * si des valeurs différentes sont fournies.
         *
         * Il y a toutefois des vérifications à produire pour certaines de ces valeurs.
         * Par exemple, la valeur email doit être valide et neuve pour pouvoir etre remplacé
         * en base. Autre exemple, l'ancien mot de passe fourni par l'utilisateur pour valider
         * l'ajout d'un nouveau mot de passe doit être lui-même valide.
         *
         * Nous commençons par créer une représentation de l'utilisateur à modifier
         *
         */


        User u = ReadUser(USER.Id);

        /**
         *
         * @User
         * A partir de cette lecture, nous mettons à jour cet utilisateur temporaire
         * si les règles ont été respectées
         *
         */
        
        // Si le nom est fourni
        if( ! userdata.Name.value.equals("")) {
            userdata.Name.Sanitize();
            u.Name = userdata.Name;
        } else {
            return 1;
        }

        // Si un email a été fourni
        if( ! userdata.Email.value.equals("")) {

            /**
             * TODO
             * Les altermoiements qui suivent sont liées à l'architecture de la classe DataEmail
             * qui n'offre pas la vérification de l'existence de l'email en base. Nous devons
             * explicitement créer un utilisateur avec cet email afin que ce dernier soit en
             * mesure d'activer sa méthode hasNewMail.
             *
             */

            User tmp = new User();
            tmp.Email = new DataEmail(userdata.Email.value);
            Console.Todo("refactorer ce problème connu");

            if(userdata.Email.isValidEmail()) {

                if(tmp.hasNewEmail()) {

                    userdata.Email.Sanitize();
                    u.Email = userdata.Email;
                    tmp = null;

                } else if(u.Email.getText().equals(tmp.Email.getText())) {

                    tmp = null;

                } else {

                    return 2;

                }

            } else {

                return 3;

            }


        }

        // Si le nouveau mot de passe n'est pas vide, et qu'il est bien le mot de passe 
        // de l'utilisateur que nous souhaitons modifier
        if(null != newpass.Hashed) {  // Le nouveau mot de passe n'est pas vide

            if(oldpass.Equals(u.Password)) { // Le mot de passe actuel requis est valide

                if( ! newpass.equals(oldpass)) { // Le nouveau et l'ancien mdp ne sont pas les mêmes

                    u.Password = newpass;

                }

            } else {

                return 4;

            }
        }

        /**
         *
         * @User
         * Reste à mettre à jour l'utilisateur courant.         *          *
         *
         *
         */


        USER.Name = u.Name;
        USER.Email = u.Email;
        USER.Password = u.Password;


        /**
         *
         * @User
         * Puis à pousser ce nouvel utilisateur vers la base
         *          *
         */

        String sql = "UPDATE Users\n"
            + "SET Name = '" + USER.Name.getText() + "',\n"
            + "Email = '" + USER.Email.getText() + "',\n"
            + "Salt = '" + USER.Password.Salt + "',\n"
            + "Password = '" + USER.Password.Hashed + "'\n"
            + "WHERE\n"
            + "ID = '" + USER.Id + "';";

        exec(sql);
        sql = null;
        
        return 0;

    }

    public void UpdateUserScore(User USER, Integer Value) {

        /**
         * @Score
         * Update de score utilisateurs
         * Pour mettre à jour un score utilisateur, il nous faut
         * une reference vers l'utilisateur (ici USER), afin de 
         * récupérer le score, et une connexion à la base de donnée.
         *
         * Cette opération s'effectue en 1 étape :
         * - Mettre à jour la base
         *
         */

        // Nous stockons la donnée
        DataScore score = new DataScore(USER.Score.getInt());

        // Nous préparons la requête
        String sql = "UPDATE Scores\n"
            + "SET value = "+score.getInt()+"\n"
            + "WHERE\n"
            + "userId = '"+USER.Id+"';" ;


        // Nous executons la requete
        exec(sql);
    }

    public User ReadUser(Integer UserId) {

        /**
         * Lecture d'un utilisateur à partir de son ID
         *
         */

        User user = new User();

        String sql = "SELECT * FROM Users WHERE id = '"+UserId+"';";
        List r = request(sql);

        Map<String, Object> map = new HashMap<String, Object>();

        try {

            map = (HashMap) r.get(0);
            user.Id = (Integer) map.get("ID");
            user.Name = new Data((String) map.get("Name"));
            user.Email = new DataEmail((String) map.get("Email"));
            user.Password = new DataPass();
            user.Password.Salt = (String) map.get("Salt");
            user.Password.Hashed = (String) map.get("Password");

        } catch (Exception e) {
            Console.Except(e, false);
        }

        String sql2 = "SELECT Value FROM Scores WHERE userId = '"+UserId+"'";
        List r2 = request(sql2);

        try {

            HashMap<String, Integer> map2 = (HashMap) r2.get(0);
            user.Score = new DataScore((Integer) map2.get("Value"));

        } catch (Exception e) {
            Console.Except(e, false);
        }

        return user; 
    }

    public String GetUserSalt(Integer UserId) {

        /**
         * Récupération du Salt d'un utilisateur à partir de son ID
         *
         */

        String sql = "SELECT Salt FROM Users WHERE ID = '"+UserId+"';";
        List r = request(sql);

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            map = (HashMap) r.get(0);
        } catch (Exception e) {
            System.err.println(e);
        }

        return (String) map.get("Salt"); 
    }

    public void DeleteUser(Integer UserId) {

        /**
         * Suppression définitive d'un utilisateur à partir de son ID
         *
         */

        String sql1 = "DELETE FROM Scores WHERE userid = '"+UserId+"';";
        exec(sql1);
        String sql2 = "DELETE FROM Users WHERE id = '"+UserId+"';";
        exec(sql2);
    }

    public Map<String, Object> SearchUser(String Email) {

        /**
         * Recherche d'un utilisateur par email
         *
         */


        String sql = "SELECT * FROM Users WHERE Email = '"+Email+"';";
        List r = request(sql);

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            map = (HashMap) r.get(0);
        } catch (Exception e) {
            System.err.println(e);
        }

        String sql2 = "SELECT value FROM Scores WHERE userId = '"+map.get("ID")+"';";
        List r_score = request(sql2);

        Map<String, Object> map_tmp = new HashMap<String, Object>();

        try {
            map_tmp = (HashMap) r_score.get(0);
        } catch (Exception e) {
            System.err.println(e);
        }

        map.put("Score", map_tmp.get("Value"));

        return map; 
    }


    public void IncreaseUserScore(User USER, int value) {

        /**
         * Augmenter le score d'un utilisateur
         *
         * L'opération se déroule en 2 temps:
         * - Incrémenter ce score de référence
         * - Mettre à jour la base
         * - Copier le nouveau score dans l'utilisateur
         *
         */

        // Nous incrémentons le score de l'utilisateur
        DataScore scoredata;
        scoredata = new DataScore(USER.Score.getInt() + value);

        // Nous mettons à jour l'utilisateur
        USER.Score = new DataScore(scoredata.getInt());

        // Nous mettons à jour la base
        UpdateUserScore(USER, scoredata.getInt());


    }

    public void DecreaseUserScore(User USER, int value) {

        /**
         * Diminuer le score d'un utilisateur
         *
         * L'opération se déroule en 2 temps:
         * - Décrémenter ce score de référence
         * - Mettre à jour la base
         * - Copier le nouveau score dabs l'utilisateur
         *
         */

        // Nous décrémentons le score de l'utilisateur
        DataScore scoredata;
        scoredata = new DataScore(USER.Score.getInt() - value);

        // Nous mettons à jour l'utilisateur
        USER.Score = scoredata;

        // Nous mettons à jour la base
        UpdateUserScore(USER, scoredata.getInt());

    }


    public void SetupDatabase() {

        /**
         * S'assurer que la database est installée
         * Lançé pendant le lancement du logiciel
         *
         */

        ConnectDb();

        String sql1 = "CREATE TABLE IF NOT EXISTS Users (ID INTEGER PRIMARY KEY AUTOINCREMENT, Email CHAR(50) NOT NULL, Name TEXT NOT NULL, Salt CHAR(30) NOT NULL, Password TEXT NOT NULL);\n"
            + ");";

        exec(sql1);

        String sql2 = "CREATE TABLE IF NOT EXISTS Scores(ID INTEGER PRIMARY KEY AUTOINCREMENT, UserId INT NOT NULL, Value INT, FOREIGN KEY(UserId) REFERENCES Users(ID));"
            + ");";

        exec(sql2);
    }

}
