package Model;

// Standard lib
import java.util.*;
import javax.swing.JPasswordField;
import java.awt.Dimension;
import java.security.SecureRandom;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.SecretKeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

// Fred lib
import Interfaces.Encryption;
import Interfaces.Dimensions;
import Interfaces.PersonalData;
import Control.*;
import Tests.*;

public class DataPass implements Encryption {

    public Boolean Referenced;
    public String Salt;
    public String Hashed;

    public DataPass() {}

    public DataPass(String s1, char[] s2) {
        Encrypt(s1, s2);
    }

    public DataPass(String s1, String s2) {
        Encrypt(s1, s2);
    }

    public DataPass(DataPass d) {

        Hashed = d.Hashed;
        Salt = d.Salt;
    }

    public DataPass(char[] s) {

        String _s = String.valueOf(s);
        
        if ( ! _s.equals("")) {
            Encrypt(_s);
        }

    }

    public DataPass(String s) {

        if ( ! s.equals("")) {
            Encrypt(s);
        }

    }

    @Override
    public String toString() {

        String res = "";
        res += "Salt : "+Salt+"\n";
        res += "Hashed : "+Hashed;
        res += "\n";
        return res;
    }

    public void Encrypt(String clear) {

        HashMap<String, String> pass = EncryptSalted(clear, null);
        Salt = pass.get("Salt");
        Hashed = pass.get("Password");
    }

    public void Encrypt(String salt, String clear) {

        HashMap<String, String> pass = EncryptSalted(clear, salt);
        Salt = pass.get("Salt");
        Hashed = pass.get("Password");

    }

    public void Encrypt(String salt, char[] clear) {

        HashMap<String, String> pass = EncryptSalted(String.valueOf(clear), salt);
        Salt = pass.get("Salt");
        Hashed = pass.get("Password");

    }

    public HashMap<String, String> GetPassword() {
        Console.Todo("Retirer GetPassword, simplifier!");
        HashMap<String, String> pass = new HashMap<String, String>();
        pass.put("Salt", Salt);
        pass.put("Hashed", Hashed);
        return pass;
    }

    public void Reference() {

        new RGPDReference("ReferenceResult : Sensitive data has been created");
        Console.Todo("Nothing writed");
        Referenced = true;

    }

    public Boolean Equals(DataPass datapass) {

        if(Hashed != null && Hashed.equals(datapass.Hashed) 
                && Salt != null && Salt.equals(datapass.Salt)) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean Equals(String clear) {

        HashMap<String, String> pass = EncryptSalted(clear, Salt);
        if(pass.get("Password").equals(Hashed)) {
            return true;
        } else {
            return false;
        }
    }

    public HashMap<String, String> EncryptSalted(String clear, String salt) {

        HashMap<String, String> map = new HashMap<String, String>();

        //TODO
        Console.Todo("@SuppressWarnings(\"unchecked'\")"); 
        @SuppressWarnings("unchecked") 
        class hashPassword {
            byte[] hash(char[] password, byte[] salt) {
                int ITERATIONS = 10000;
                int KEY_LENGTH = 256;
                PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
                Arrays.fill(password, Character.MIN_VALUE);
                try {
                    SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
                    return skf.generateSecret(spec).getEncoded();
                } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                    throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
                } finally {
                    spec.clearPassword();
                }
            }
        }

        if(null == salt) {
            Random RANDOM = new SecureRandom();
            StringBuilder returnValue = new StringBuilder(30);
            String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            for (int i = 0; i < 30; i++) {
                returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
            }

            salt = new String(returnValue);

        }

        hashPassword hp = new hashPassword();
        byte[] securePassword = hp.hash(clear.toCharArray(), salt.getBytes());
        clear = null;
        String hashedPassword = Base64.getEncoder().encodeToString(securePassword);

        map.put("Salt", salt);
        map.put("Password", hashedPassword);

        return map;
    }

}
