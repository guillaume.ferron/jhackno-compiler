package Model;

// Fred lib
import Interfaces.SecMail;

// Standard lib
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class DataEmail extends Data implements SecMail {

    public DataEmail() {}

    public DataEmail(String s) {
        super(s);
    }

    public Boolean isValidEmail() {

        String Value = getText();

        //OWASP Ref
        String EMAIL_REGEX =
            "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*" + //identifier
            "@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"; //domain

        Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

        if (Value == null) {
            return false;
        }

        Matcher matcher = EMAIL_PATTERN.matcher(Value);
        return matcher.matches();
    }

}
