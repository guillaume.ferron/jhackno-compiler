# How to use?

- Edit the file ./testfile
- To launch the calculator => ./start.sh
- To clean .class and ANTLR automatically built files => ./clean.sh

## Files
*source-code refers to "Source Code of your own"*
- testfile => custom source-code 
- Calc.g4 => grammar declaration and parsers for source-code
- CalcLexers.g4  => separated lexers for source-code
- Calc.java => main class
- CustomCalcVisitor.java => visitor for parser
- clean.sh => reset example
- compile.sh => compile example
- run.sh => run example
