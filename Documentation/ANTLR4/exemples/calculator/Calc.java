import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.FileInputStream;
import java.io.InputStream;

public class Calc {

    public static void main (String[] args) throws Exception {
        //get input
        FileInputStream file_content = new FileInputStream("testfile");
        ANTLRInputStream input = new ANTLRInputStream(file_content);

        //build lexer
        CalcLexer lexer = new CalcLexer(input);

        //build tokens stream
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        //build parser
        CalcParser parser = new CalcParser(tokens);

        //build tree
        ParseTree tree = parser.prog();

        //display tree
        //System.out.println(tree.toStringTree(parser));

        CustomCalcVisitor visitor = new CustomCalcVisitor();
        visitor.visit(tree);
    }

}

