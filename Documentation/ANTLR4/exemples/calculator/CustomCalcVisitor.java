import java.util.HashMap;
import java.util.Map;

public class CustomCalcVisitor extends CalcBaseVisitor<Integer> {

    Map<String, Integer> memory = new HashMap<String, Integer>();

    @Override
    public Integer visitAssign(CalcParser.AssignContext ctx) {
        String id = ctx.ID().getText();
        int value = visit(ctx.expr());
        memory.put(id, value);
        return value;
    }

    @Override
    public Integer visitCode(CalcParser.CodeContext ctx) {
        Integer value = visit(ctx.expr());
        System.out.println(value);
        return 0;
    }

    @Override
    public Integer visitInt(CalcParser.IntContext ctx) {
        return Integer.valueOf(ctx.INT().getText());
    }

    @Override
    public Integer visitId(CalcParser.IdContext ctx) {
        String id = ctx.ID().getText();
        if (memory.containsKey(id)) return memory.get(id);
        return 0;
    }


    @Override
    public Integer visitOperation1(CalcParser.Operation1Context ctx) {
        int left = visit(ctx.expr(0));
        int right = visit(ctx.expr(1));
        if (ctx.op.getType() == CalcParser.MUL) return left * right;
        return left / right;
    }

    @Override
    public Integer visitOperation2(CalcParser.Operation2Context ctx) {
        int left = visit(ctx.expr(0));
        int right = visit(ctx.expr(1));
        if (ctx.op.getType() == CalcParser.SUB) return left - right;
        return left + right;
    }


    @Override
    public Integer visitParentheses(CalcParser.ParenthesesContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Integer visitClear(CalcParser.ClearContext ctx) {
        memory = new HashMap<String, Integer>();
        return 0;
    }
}
