lexer grammar CalcLexers;

ID : [a-zA-Z_]+;
INT : [0-9]+;
NEWLINE : '\r'? '\n';
WS : [ \t]+ -> skip;
