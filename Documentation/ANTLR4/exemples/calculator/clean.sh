#!/bin/bash

rm CalcVisitor*.java CalcLexer*.java CalcParser*.java CalcBaseVisitor*.java > /dev/null  2>&1
rm Calc*Listener.java > /dev/null 2>&1
rm *.interp > /dev/null  2>&1
rm *.tokens > /dev/null  2>&1
rm *.class > /dev/null  2>&1
