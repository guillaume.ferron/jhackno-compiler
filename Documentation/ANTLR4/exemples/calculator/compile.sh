#!/bin/bash

echo "Calculator@Mimo -- Compilation started"
antlr4 -no-listener -visitor Calc.g4
javac *.java
echo "Calculator@Mimo -- Compilation terminated"
